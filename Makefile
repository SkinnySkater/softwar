CC		=	gcc -g3

#CFLAGS	=	-Wall -Wextra -Werror  -W -g3

LDFLAGS	=	-L/usr/local/lib/
INCLUDES= 	-I/usr/local/
LIBS 	= 	-lzmq -lczmq -lpthread

SRC		=	src/server/mtserver2.c src/server/notification.c src/server/util.c src/server/action.c\
			src/server/check_input.c src/main.c

OBJ		=	$(SRC:%.c=%.o)


SERVER	=	server
CLIENT	=	client


.PHONY	:	fclean clean all re

all	:	$(SERVER) $(CLIENT)

$(SERVER)	:	$(OBJ)
	$(CC) $(SRC) $(HELP) -o $@ $(LIBS)


clean	:
	$(RM) $(OBJ) *.swp *~ t_* src/client/*.pyc 

fclean	:	clean
	$(RM) $(SERVER)

re	:	fclean all