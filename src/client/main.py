import pygame
import time
import random
import sys
sys.path.insert(0, './class')
from constantes import (IMAGE_ICON, SCREEN_SIZE, BACKGROUND_MENU_IMAGE,
                        SCREEN_WIDTH, SCREEN_HEIGHT, BLACK,
                        RECTANGLE_FOND_TEXTE_MENU, GREEN2,
                        TEXT_FONT_TITLE_MENU, WHITE, TEXT_FONT_MODS_MENU,
                        MAGE, TOTAL_AREA_SIZE, SPRITE_SIZE, POSITION_X_AREA,
                        POSITION_Y_AREA, ADAM, BILL, NINJA, SIZE_MAP,
                        ENERGY_BALL, EXPLOSION, POSITION_X_PLAYER_ONE,
                        POSITION_Y_PLAYER_ONE, POSITION_X_PLAYER_TWO,
                        POSITION_Y_PLAYER_TWO, POSITION_X_PLAYER_THREE,
                        POSITION_Y_PLAYER_THREE, POSITION_X_PLAYER_FOUR,
                        POSITION_Y_PLAYER_FOUR, PLAYER_CHOICE)
from utilities import (display_area, display_credits, display_text,
                       matrix_background, hit, make_position_item,
                       display_items, show_balls, anime_explosion,
                       action_item, show_players_status, show_skull_head)

from Player import Player
from Ai import AI

# Initialisation du jeu
pygame.init()

# Cache la souris
pygame.mouse.set_visible(False)

# Initialisation de l'icone dans la barre de taches
pygame.display.set_icon(IMAGE_ICON)

# Initialisation des frames
frame = pygame.time.Clock()

# Initialisation de la fenetre du jeu en donnant la largeur
# et la hauteur de la fenetre dans laquelle le jeu va s'afficher
screen = pygame.display.set_mode(SCREEN_SIZE, pygame.RESIZABLE)

# Variable pour savoir si le jeu est fini
finished = False

# Booléen pour savoir si le joueur n'est pas apparue
# au moin une fois pour ne pas le faire ré-apparaitre tout le temps
already_appear = False


# !##################################
# !          DEBUT  MENU            #
# !##################################
# Variable pour savoir si l'on est dans le menu
menu = True
# Compteur pour les images de fond du menu
counter_menu = 0
# !##################################
# !          FIN  MENU              #
# !##################################

# Le nombre de temps avant de repeter la boucle
# (pour quand on reste appuye sur une touche)
pygame.key.set_repeat(400, 100)

# Tous les items sur la map
items = []

# Compteur pour savoir quand afficher les items
compteur_item = 0

# Tous les [joueurs(objet Pygame)], [les positions], {l'energie(vie), le stamina(action) et les noms}
players = []

# Position de toutes les boules d'energie
balls = []
# Compteur de boules lance
counter_balls = 0

# Tableau pour savoir ou faire l'animation d'explosion
position_explosion = []

# Tableau de la ou les tete de mort apparaissent
dead_positions = []

# Mode pour savoir si on joue ou si les AI jouent
mode_ai = False

# Compteur avant de finir le jeu
counter_before_close_the_game = 50
counter_before_end_the_game = 0

# Minuteur pour afficher le status des joeurs
counter_before_status_disapear = 0

# Booleen pour savoir si quelqu'un a gagne
winned = False

# Boucle principale du jeu
# pour savoir si le jeu est fini
while finished is not True:
    # Bouche pour savoir si on
    # est encore dans le menu
    while menu is not False:
        for event in pygame.event.get():
            # Mettre toutes les touches dans une liste
            pressedKey = pygame.key.get_pressed()
            # Si on appuie sur F3, Echap ou sur la croix pour quitter
            if (event.type is pygame.QUIT or pressedKey[pygame.K_ESCAPE] or
                    pressedKey[pygame.K_q]):
                # Mettre fin a la boucle du menu
                # ainsi qu'a la boucle du jeu
                menu = False
                finished = True
                print('Le jeu a ete quitte (boucle menu)')
            # Si on appuie sur F1 on accede au jeu en quittant la
            # boucle du menu (mode AI)
            elif pressedKey[pygame.K_F1]:
                menu = False
                mode_ai = True
            # Si on appuie sur F2 on affiche les credits du jeu
            # dans la console
            elif pressedKey[pygame.K_F2]:
                display_credits()
            # Si on appuie sur F3 on accede au jeu en quittant la
            # boucle du menu (mode player)
            elif pressedKey[pygame.K_F3]:
                mode_ai = False
                menu = False
        # Chargement de l'image provenant d'une liste
        # contenant toutes les frames du background
        background_image = BACKGROUND_MENU_IMAGE[counter_menu]
        image_background = pygame.image.load(background_image).convert()
        image_background = pygame.transform.scale(image_background,
                                                  (SCREEN_WIDTH,
                                                   SCREEN_HEIGHT))
        # On colle l'image de fond sur la fenetre
        screen.blit(image_background, (0, 0))
        # Incrementation du compteur de la liste de frame
        counter_menu += 1
        # Si le compteur est arrive a la taille du tableau
        # des images du menu alors reinitialisation du compteur
        if counter_menu >= len(BACKGROUND_MENU_IMAGE) - 1:
            counter_menu = 0
        # Affichage du rectangle servant de fond au texte du menu
        pygame.draw.rect(screen, BLACK, RECTANGLE_FOND_TEXTE_MENU)
        # Affichage du texte dans le rectangle
        display_text('SOFTWAR', GREEN2, screen,
                     position_x=(SCREEN_WIDTH / 5), position_y=10,
                     text_size=150, text_font=TEXT_FONT_TITLE_MENU)
        display_text('Play AI mode (F1)', WHITE, screen, True,
                        text_size=30, text_font=TEXT_FONT_MODS_MENU)
        display_text('Play player mode (F3)', WHITE, screen, True,
                    -30, 70, text_size=30, text_font=TEXT_FONT_MODS_MENU)
        display_text('Credits (F2)', WHITE, screen, True,
                    -10, 140, text_size=30, text_font=TEXT_FONT_MODS_MENU)
        display_text('Quit (q or Escape)', WHITE, screen, True,
                    -10, 210, text_size=30, text_font=TEXT_FONT_MODS_MENU)
        # Limiter 30 images par seconde pour
        # pas que ca defile trop vite
        frame.tick(60)
        # Mise a jour de la fenetre
        pygame.display.flip()

    for event in pygame.event.get():

        # Mettre toutes les touches dans une liste
        pressedKey = pygame.key.get_pressed()

        # Si on appuie sur Echap ou sur la croix pour quitter
        if event.type is pygame.QUIT or pressedKey[pygame.K_ESCAPE]:
            # Mettre fin a la boucle principale du jeu
            # en mettant la variable finished a True
            # donc mettre fin au jeu
            finished = True
            print('Le jeu a ete quitte (boucle jeu)')
        
    if already_appear is False:
        # Chargement des joueurs et incrustation dans la fenetre
        # une seule fois quand le jeu se lance
        if mode_ai is False:
            joueur_un = Player(PLAYER_CHOICE[0], 100, 50, '0x01')
        else:
            joueur_un = AI(PLAYER_CHOICE[0], 100, 50, '0x01')
        joueur_deux = AI(PLAYER_CHOICE[1], 100, 50, '0x02')
        joueur_trois = AI(PLAYER_CHOICE[2], 100, 50, '0x03')
        joueur_quatre = AI(PLAYER_CHOICE[3], 100, 50, '0x04')

        player_one = joueur_un.appear(POSITION_X_PLAYER_ONE, POSITION_Y_PLAYER_ONE)
        player_two = joueur_deux.appear(POSITION_X_PLAYER_TWO, POSITION_Y_PLAYER_TWO)
        player_three = joueur_trois.appear(POSITION_X_PLAYER_THREE, POSITION_Y_PLAYER_THREE)
        player_four = joueur_quatre.appear(POSITION_X_PLAYER_FOUR, POSITION_Y_PLAYER_FOUR)

        # Switch du booléen pour les apparitions
        already_appear = True

        players = [
            player_one,
            player_two,
            player_three,
            player_four
        ]
    
    # Commande du mode player
    if mode_ai is False:
        if pressedKey[pygame.K_c]:
            # Si la  touche du haut est appuyee
            if pressedKey[pygame.K_UP]:
                player_one = joueur_un.look_up(player_one)

            # Si la  touche du bas est appuyee
            elif pressedKey[pygame.K_DOWN]:
                player_one = joueur_un.look_down(player_one)

            # Si la  touche de gauche est appuyee
            elif pressedKey[pygame.K_LEFT]:
                player_one = joueur_un.look_left(player_one)

            # Si la touche de droite est appuyee
            elif pressedKey[pygame.K_RIGHT]:
                player_one = joueur_un.look_right(player_one)

        else:
            # Si la  touche du haut est appuyee
            if pressedKey[pygame.K_UP]:
                player_one = joueur_un.go_up(player_one)

            # Si la  touche du bas est appuyee
            elif pressedKey[pygame.K_DOWN]:
                player_one = joueur_un.go_down(player_one)

            # Si la  touche de gauche est appuyee
            elif pressedKey[pygame.K_LEFT]:
                player_one = joueur_un.go_left(player_one)

            # Si la touche de droite est appuyee
            elif pressedKey[pygame.K_RIGHT]:
                player_one = joueur_un.go_right(player_one)

            # Si la touche 'b' est appuyee
            elif pressedKey[pygame.K_b]:
                player_one = joueur_un.go_jump(player_one)

            # Si la touche 'v' est appuyee
            elif pressedKey[pygame.K_v]:
                player_one[2]['action'] -= 1
                player_one[2]['energy'] -= 0.5
                energy_ball = list(joueur_un.attack())
                balls.append(energy_ball)


    # Couleur du fond derriere l'arene de combat
    screen.fill(BLACK)
    # Defillement d'etoile
    matrix_background(screen)
    # Affichage de la zone de combat
    display_area(screen, SIZE_MAP)
    # Placement des joueur
    if already_appear is True:
        for player in players:
            screen.blit(player[0], player[1])

        # faire appraitre les tetes de mort et le joueur qui a gagne
        if len(dead_positions) > 0:
            if len(dead_positions) > 2:
                counter_before_end_the_game +=1
                # Quand un joueur gagne
                if counter_before_end_the_game >= 10:
                    winned = True
                    print("F I N I S H !")
                    counter_before_end_the_game = 10
            # Chaque position faire apparaitre une tete de mort
            for position in dead_positions:
                    show_skull_head(screen, position) 

        # Mouvement des AIs
        if player_one[2]['energy'] > 0: # Server
            if mode_ai is False:
                player_one = player_one
            else:
                player_one = joueur_un.move_auto(players[0])
        else:
            if player_one[1] not in dead_positions:
                dead_positions.append(player_one[1])
        if player_two[2]['energy'] > 0: # Server
            player_two = joueur_deux.move_auto(players[1])
        else:
            if player_two[1] not in dead_positions:
                dead_positions.append(player_two[1])
        if player_three[2]['energy'] > 0: # Server
            player_three = joueur_trois.move_auto(players[2])
        else:
            if player_three[1] not in dead_positions:
                dead_positions.append(player_three[1])
        if player_four[2]['energy'] > 0: # Server
            player_four = joueur_quatre.move_auto(players[3])
        else:
            if player_four[1] not in dead_positions:
                dead_positions.append(player_four[1])

        players = [
            player_one,
            player_two,
            player_three,
            player_four
        ]

        # Retourne la position des boules d'energie et la position des joueurs
        balls, players = show_balls(screen, balls, players,
                                                    position_explosion)

        # Si le joueur tire il perd de l'energie et du stamina
        for i, player in enumerate(players):
            if len(player) > 3:
                balls.append(player[3])
                player[2]['energy'] -= 0.5
                player[2]['action'] -= 1

    # Fais apparaitre l'animation de l'explosion
    if len(position_explosion) > 0:
        for explosion in position_explosion:
            for player in players:
                if explosion[1] == player[1]:
                    player[2]['energy'] -= 1
        position_explosion = anime_explosion(screen, position_explosion)


    # Incrementer le compteur des items, si le compteur est a 100 il se remet a 0
    # Ce compteur permet d'afficher des items tous les 100 tours aleatoirement
    compteur_item += 1
    if compteur_item is 100:
        item_position = make_position_item()
        items.append(item_position)
        compteur_item = 0

    # Affiche les items , les enleve si un joueurs en touche un autre
    if len(items) > 0:
        items, players = action_item(items, players, screen)

    # Met a 0 si l'energie ou le stamina d'un joueur est en dessous de 0
    for player in players:
        if player[2]['energy'] < 0:
            player[2]['energy'] = 0
        if player[2]['action'] < 0:
            player[2]['action'] = 0

    # Appuyer sur la touche 's' pour voir le status des joueurs (vie, nom..)
    if pressedKey[pygame.K_s]:
        if counter_before_status_disapear is 0:
            counter_before_status_disapear += 10
    if counter_before_status_disapear > 0 :
        # Affiche l'energie et le stamina des joueurs au dessus de leur tete
        show_players_status(players, screen)
        counter_before_status_disapear -= 1

    # Reduction des frames
    frame.tick(30)

    # Mise a jour de l'ecran
    pygame.display.flip()

    # Si le jeu est fini attendre 50 tours avant de fermer la fenetre
    if winned is True and counter_before_end_the_game is 10:
        if counter_before_close_the_game is 0:
            finished = True
        else:
            counter_before_close_the_game -= 1