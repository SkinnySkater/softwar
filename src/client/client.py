import time
import zmq
import sys

def generate_id(id):
    identification = "#0x0%d" % (id)
    return identification

def suscribing(socket, subscriber, id_client):
    id_ = 2
    id_client = generate_id(id_)
    while True:
        socket.send("%s|%s" % ("identify", id_client))
        message = socket.recv()
        print("-Received [%s]" % (message))
        if message.find("ko") > -1:
            id_ += 1
            id_client = generate_id(id_)
        else:
            [address, contents] = subscriber.recv_multipart()
            print("[%s] %s" % (address, contents))
            break;

    while True:
        [address, contents] = subscriber.recv_multipart()
        print("[%s] %s" % (address, contents))
        if contents == "ready":
            break;

def main():
    # Prepare our context and publisher
    context    = zmq.Context()
    # First, connect our subscriber socket
    subscriber = context.socket(zmq.SUB)
    subscriber.connect("tcp://localhost:5563")
    subscriber.setsockopt(zmq.SUBSCRIBE, b"UPDATE")

    time.sleep(1)

    #Set up socket for Request-reply pattern
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5555")

    id_client = ""
    while True:
        if id_client == "":
            suscribing(socket, subscriber, id_client)
        # Read envelope with address from publisher
        contents = subscriber.recv()
        print("[%s]" %  contents)
        if contents == b'DEAD':
            break
        #Try to send request and print the answer from Request-reply pattern
        socket.send(b"Hello Fucking World !")
        message = socket.recv()
        print("Received [%s]" % (message))

    # We never get here but clean up anyhow
    subscriber.close()
    context.term()

if __name__ == "__main__":
    main()
