import pygame
import random
from Player import Player
import sys
sys.path.insert(0, '..')
from constantes import SIZE_MAP

class AI(Player):
    """
    Class pour instancier, bouger les AIs
    """
    def __init__(self, images, action, energy, name):
        super().__init__(images, action, energy, name)
        self.__random_number_for_move = 0
        self.__how_many_move = 0
        self.__counter_before_move = 0

    def move_auto(self, player):
        """
        Mouvement automatique du personnage
        """
        # Fait un mouvement quand le compteur est a 5
        if self.__counter_before_move is 5:
            # le compteur est ensuite reinitialise a 0
            self.__counter_before_move = 0
            # Generer un nombre aleatoire pour savoir
            # combien de fois le personnage bouge dans un sens
            if self.__how_many_move is 0:
                self.__how_many_move = random.randint(1, int(SIZE_MAP / 2))
                # Genere un chiffre pour savoir quel mouvement
                # le personnage va faire
                self.__random_number_for_move = random.randint(0, 9)
            # Decremente le compteur a chaque fois que
            # le personnage fait un mouvement
            self.__how_many_move -= 1
            # Fait le mouvement correspondant au chiffre tire au sort
            if self.__random_number_for_move is 0:
                then = self.go_up(player)
            elif self.__random_number_for_move is 1:
                then = self.go_right(player)
            elif self.__random_number_for_move is 2:
                then = self.go_down(player)
            elif self.__random_number_for_move is 3:
                then = self.go_left(player)
            elif self.__random_number_for_move is 4:
                if self.__how_many_move <= 2:
                    then = self.go_jump(player)
                else:
                    then = self.image_now, self.position, { 'action': player[2]['action'],
                                                            'energy': player[2]['energy'],
                                                            'name': player[2]['name']
                                                            }
            elif self.__random_number_for_move is 5:
                then = self.look_up(player)
            elif self.__random_number_for_move is 6:
                then = self.look_right(player)
            elif self.__random_number_for_move is 7:
                then = self.look_down(player)
            elif self.__random_number_for_move is 8:
                then = self.look_left(player)
            elif self.__random_number_for_move is 9:
                if self.__how_many_move > 2:
                    self.__how_many_move = 1
                energy_ball = list(self.attack())
                then = self.image_now, self.position, { 'action': player[2]['action'],
                                                        'energy': player[2]['energy'],
                                                        'name': player[2]['name']
                                                        }, energy_ball
        else:
            then = self.image_now, self.position, { 'action': player[2]['action'],
                                                    'energy': player[2]['energy'],
                                                    'name': player[2]['name']
                                                    }

        # Incremente le compteur pour effectuer un mouvement
        self.__counter_before_move += 1

        return then