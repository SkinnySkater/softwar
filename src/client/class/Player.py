import pygame
from EnergyBall import EnergyBall
import sys
sys.path.insert(0, '..')
from constantes import (SPRITE_SIZE, ENERGY_BALL, POSITION_X_AREA,
                        TOTAL_AREA_SIZE, POSITION_Y_AREA, WHITE)

class Player(object):
    """
    Class pour instancier, bouger les joueurs
    """
    def __init__(self, images, action, energy, name):
        self.images = images
        self.image_now = ''
        self.action = action
        self.energy = energy
        self.name = name
        self.position = []
        self.__image_up = 0
        self.__image_down = 0
        self.__image_right = 0
        self.__image_left = 0
        self.__which_direction = 2

    def limit_zone(self):
        """
        Empeche le joueur de sortir de la zone de combat
        """
        # Variables
        right_limit = POSITION_X_AREA + TOTAL_AREA_SIZE - SPRITE_SIZE
        left_limit = POSITION_X_AREA
        up_limit = POSITION_Y_AREA
        down_limit = POSITION_Y_AREA + (TOTAL_AREA_SIZE - SPRITE_SIZE)

        # Si le joueur depasse a droite
        if self.position[0] > right_limit:
            self.position[0] = right_limit
        # Si le joueur depasse a gauche
        elif self.position[0] < left_limit:
            self.position[0] = left_limit
        # Si le joueur depasse en haut
        elif self.position[1] < up_limit:
            self.position[1] = up_limit
        # Si le joueur depasse en bas
        elif self.position[1] > down_limit:
            self.position[1] = down_limit
        else:
            pass

    def appear(self, position_x, position_y):
        """
        Faire apparaitre le personnage a un point
        x et y donné en parametre
        """
        first_appear = pygame.image.load(self.images[4])
        first_appear = pygame.transform.scale(first_appear, (SPRITE_SIZE,
                                                             SPRITE_SIZE))
        first_appear.convert_alpha()
        self.position = [position_x, position_y]
        self.image_now = first_appear

        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }

    def go_up(self, player):
        """
        Bouger le personnage
        vers le haut
        """

        if self.__image_up is 1:
            self.__image_up = 0
        else:
            self.__image_up = 1

        playerimage = pygame.image.load(self.images[self.__image_up])
        playerimage = pygame.transform.scale(playerimage, (SPRITE_SIZE,
                                                           SPRITE_SIZE))
        playerimage.set_colorkey(WHITE)
        position = [
            self.position[0],
            self.position[1] - SPRITE_SIZE
            ]

        self.image_now = playerimage
        self.position = position
        self.__which_direction = 0
        self.energy = player[2]['energy']
        self.action = player[2]['action'] - 1
        self.limit_zone()

        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }

    def go_down(self, player):
        """
        Bouger le personnage
        vers le bas
        """

        if self.__image_down is 4:
            self.__image_down = 5
        else:
            self.__image_down = 4

        playerimage = pygame.image.load(self.images[self.__image_down])
        playerimage = pygame.transform.scale(playerimage,
                                             (SPRITE_SIZE, SPRITE_SIZE))
        playerimage.set_colorkey(WHITE)
        position = [
            self.position[0],
            self.position[1] + SPRITE_SIZE
            ]

        self.image_now = playerimage
        self.position = position
        self.__which_direction = 2
        self.energy = player[2]['energy']
        self.action = player[2]['action'] - 1
        self.limit_zone()

        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }

    def go_left(self, player):
        """
        Bouger le personnage
        vers la gauche
        """

        if self.__image_left is 6:
            self.__image_left = 7
        else:
            self.__image_left = 6

        playerimage = pygame.image.load(self.images[self.__image_left])
        playerimage = pygame.transform.scale(playerimage,
                                             (SPRITE_SIZE, SPRITE_SIZE))
        playerimage.set_colorkey(WHITE)
        position = [
            self.position[0] - SPRITE_SIZE,
            self.position[1]
            ]

        self.image_now = playerimage
        self.position = position
        self.__which_direction = 3
        self.energy = player[2]['energy']
        self.action = player[2]['action'] - 1
        self.limit_zone()

        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }

    def go_right(self, player):
        """
        Bouger le personnage
        vers la droite
        """

        if self.__image_right is 2:
            self.__image_right = 3
        else:
            self.__image_right = 2

        playerimage = pygame.image.load(self.images[self.__image_right])
        playerimage = pygame.transform.scale(playerimage, (SPRITE_SIZE,
                                                           SPRITE_SIZE))
        playerimage.set_colorkey(WHITE)
        position = [
            self.position[0] + SPRITE_SIZE,
            self.position[1]
            ]

        self.image_now = playerimage
        self.position = position
        self.__which_direction = 1
        self.energy = player[2]['energy']
        self.action = player[2]['action'] - 1
        self.limit_zone()

        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }

    def go_jump(self, player):
        """
        Faire un saut (teleportation)
        """
        # Savoir dans quelle direction est le personnage 
        # pour effectuer le saut dans le bon sens
        if self.__which_direction is 0:
            position = [
                self.position[0],
                self.position[1] - (SPRITE_SIZE * 2)
                ]
        elif self.__which_direction is 2:
            position = [
                self.position[0],
                self.position[1] + (SPRITE_SIZE * 2)
                ]
        elif self.__which_direction is 3:
            position = [
                self.position[0] - (SPRITE_SIZE * 2),
                self.position[1]
                ]
        elif self.__which_direction is 1:
            position = [
                self.position[0] + (SPRITE_SIZE * 2),
                self.position[1]
                ]

        self.energy = player[2]['energy'] - 2
        self.action = player[2]['action']

        # Reinitialiser la position
        self.position = position
        # Verifier que l'on sort pas de la limite
        # de la zone de combat
        self.limit_zone()

        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }

    def attack(self):
        """
        Lance une attaque (boule d'energie)
        """
        # Savoir ou je fais apparaitre la boule d'energie
        # dans notre cas elle apparait a la position du joueur
        # qui lance la boule
        position_ball_x = self.position[0]
        position_ball_y = self.position[1]

        energy_ball = EnergyBall(position_ball_x, position_ball_y, self.__which_direction)
        energy_ball = energy_ball.appear()

        return energy_ball

    def look_up(self, player):
        """
        Regarder en haut
        """
        self.__image_up = 0

        playerimage = pygame.image.load(self.images[self.__image_up])
        playerimage = pygame.transform.scale(playerimage, (SPRITE_SIZE,
                                                           SPRITE_SIZE))
        playerimage.set_colorkey(WHITE)
        position = [
            self.position[0],
            self.position[1]
            ]

        self.image_now = playerimage
        self.position = position
        self.__which_direction = 0
        self.energy = player[2]['energy']
        self.action = player[2]['action'] - 0.5
        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }
    
    def look_down(self, player):
        """
        Regarder en bas
        """

        self.__image_down = 5

        playerimage = pygame.image.load(self.images[self.__image_down])
        playerimage = pygame.transform.scale(playerimage,
                                             (SPRITE_SIZE, SPRITE_SIZE))
        playerimage.set_colorkey(WHITE)
        position = [
            self.position[0],
            self.position[1]
            ]

        self.image_now = playerimage
        self.position = position
        self.__which_direction = 2
        self.energy = player[2]['energy']
        self.action = player[2]['action'] - 0.5
        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }
    
    def look_left(self, player):
        """
        Regarder a gauche
        """

        self.__image_left = 6

        playerimage = pygame.image.load(self.images[self.__image_left])
        playerimage = pygame.transform.scale(playerimage,
                                             (SPRITE_SIZE, SPRITE_SIZE))
        playerimage.set_colorkey(WHITE)
        position = [
            self.position[0],
            self.position[1]
            ]

        self.image_now = playerimage
        self.position = position
        self.__which_direction = 3
        self.energy = player[2]['energy']
        self.action = player[2]['action'] - 0.5
        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }

    def look_right(self, player):
        """
        Regarder a droite
        """     
        self.__image_right = 2

        playerimage = pygame.image.load(self.images[self.__image_right])
        playerimage = pygame.transform.scale(playerimage, (SPRITE_SIZE,
                                                           SPRITE_SIZE))
        playerimage.set_colorkey(WHITE)
        position = [
            self.position[0],
            self.position[1]
            ]

        self.image_now = playerimage
        self.position = position
        self.__which_direction = 1
        self.energy = player[2]['energy']
        self.action = player[2]['action'] - 0.5

        return self.image_now, self.position, {
                                                'action': self.action,
                                                'energy': self.energy,
                                                'name': self.name
                                                }