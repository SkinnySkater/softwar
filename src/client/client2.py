import time
import zmq
import sys
from random import randint, random

def generate_id(id=1):
    identification = "#0x0%d" % (randint(0,3))
    return identification

def main():
    id_client = ""
    # Prepare our context and publisher
    context    = zmq.Context()
    # First, connect our subscriber socket
    subscriber = context.socket(zmq.SUB)
    subscriber.connect('tcp://localhost:5561')
    subscriber.setsockopt(zmq.SUBSCRIBE, b'')

    time.sleep(1)

    # Second, synchronize with publisher
    syncclient = context.socket(zmq.REQ)
    syncclient.connect('tcp://localhost:5562')

    # Set up socket for Request-reply pattern
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5555")

    # send a synchronization request
    rep = "%s|%s" % ("identify", generate_id())
    syncclient.send(rep.encode('ascii'))
    # wait for synchronization reply
    contents = syncclient.recv()
    if (contents.decode().split("|")[0] == "ko"):
        print(contents)
        sys.exit()
    else:
        id_client = contents.decode().split("|")[1]
        #clear id_client
        id_client = id_client[:5]
        print("Name:%s" % id_client)
    print("Waiting for all player to connect...")

    i = 0
    while True:
        # Read envelope with address from publisher
        contents = subscriber.recv()
        print("[%s] %s" % ("server", contents))
        if contents == b'GAMECHECK':
            socket.send(id_client)
        if contents == b'DEAD':
            break
        if 'PD|' + str(id_client) in contents:
            break
        #Try to send request and print the answer from Request-reply pattern
        #rep = "Hello world !" + str(i)
        rep = "jump|" + id_client
        socket.send_string(rep)
        message = socket.recv()
        print("Received [%s]" % (message))
        i += 1

    # We never get here but clean up anyhow
    subscriber.close()
    context.term()

if __name__ == "__main__":
    main()
