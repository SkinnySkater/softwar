import pygame
import random

screen = pygame.display.set_mode([680, 480])
height = pygame.display.Info().current_h
width = pygame.display.Info().current_w
pygame.display.set_caption('Window Caption')
clock = pygame.time.Clock()

#create the locations of the stars for when we animate the background
star_field_slow = []
star_field_medium = []
star_field_fast = []

for slow_stars in range(50): #birth those plasma balls, baby
    star_loc_x = random.randrange(0, width)
    star_loc_y = random.randrange(0, height)
    star_field_slow.append([star_loc_x, star_loc_y])

for medium_stars in range(35):
    star_loc_x = random.randrange(0, width)
    star_loc_y = random.randrange(0, height)
    star_field_medium.append([star_loc_x, star_loc_y])

for fast_stars in range(15):
    star_loc_x = random.randrange(0, width)
    star_loc_y = random.randrange(0, height)
    star_field_fast.append([star_loc_x, star_loc_y])

#define some commonly used colours
WHITE = (255, 255, 255)
LIGHTGREY = (192, 192, 192)
DARKGREY = (128, 128, 128)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
GREEN2 = (154, 255, 0)
GREEN3 = (0, 255, 154)
YELLOW = (255, 255, 0)


#create the window
pygame.init()

app_is_alive = True

while app_is_alive:
    rand = random.randrange(0, width)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            print "Exiting... All hail the void!"
            app_is_alive = False #murderer!
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            print "Exiting... All hail the void!"
            app_is_alive = False #murderer!

    screen.fill(BLACK)

    #animate some motherfucking stars
    for star in star_field_slow:
        star[1] += 1
        if star[1] > height:
            star[0] = random.randrange(0, width)
            star[1] = random.randrange(-20, -5)
        pygame.draw.circle(screen, GREEN3, star, 3)

    for star in star_field_medium:
        star[1] += 4
        if star[1] > height:
            star[0] = random.randrange(0, width)
            star[1] = random.randrange(-20, -5)
        pygame.draw.circle(screen, GREEN2, star, 2)

    for star in star_field_fast:
        star[1] += 8
        if star[1] > height:
            star[0] = random.randrange(0, width)
            star[1] = random.randrange(-20, -5)
        pygame.draw.circle(screen, GREEN, star, 1)

    #redraw everything
    pygame.display.flip()

    #set frames per second
    clock.tick(30)

#quit gracefully
pygame.quit()