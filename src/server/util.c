#include "notification.h"

/*
*	Return a format answer following : status|data
*	Where Status  = ok or ko
*	And data the matching data to the request, could be NULL 
*/
char 						*generate_answer(char *status, char* data)
{
    char *answer;
    answer = malloc(sizeof(char) * 50);

    sprintf(answer, "%s%s%s", status, SEPARATOR_CMD, data);
    return (answer);
}

char 						*get_cmd_from_client(char *string)
{
    char *data;
    printf("%s\n", string);
    data = strtok(string, SEPARATOR_CMD);
    printf("cmd:%s\n", data);
    return (data);
}

char 						*get_arg_from_client(char *string)
{
    char *data;
    printf("-%s\n", string);
    strtok(string, SEPARATOR_CMD);
    data = strtok(NULL, SEPARATOR_CMD);
    printf("arg:%s\n", data);
    return (data);
}

/*
* Return a player given the id
* Return Null otherwise
*/
player_t 					*get_player(zlist_t * list, char *name)
{
	player_t *p;
    p = zlist_first(list);
    if (!strcmp(p->name, name))
    	return (p);
    while (p != NULL)
    {
        if (!strcmp(p->name, name))
    		return p;
        p = zlist_next(list);
    }
    return (NULL);
}

player_t 					*get_playerByPos(zlist_t * list, int x , int y)
{
	player_t *p;
    p = zlist_first(list);
    if (p->energy > 0 && (p->x == x && p->y == y))
    	return (p);
    while (p != NULL)
    {
        if (p->energy > 0 && (p->x == x && p->y == y))
    		return (p);
        p = zlist_next(list);
    }
    return (NULL);
}

/*
* WWarn about 
*/
int 					check_winner(zlist_t * list)
{
	player_t *p;
    p = zlist_first(list);
    int res;
    res = 0;

    if (p->energy <= 0)
    	res++;
    while (p != NULL)
    {
        if (p->energy <= 0)
    		res++;	
        p = zlist_next(list);
    }
    printf("res %d\n", res);
    if (res == 4)
    	return 1;
    else
    	return 0;
}

/*
* After every cycle decrease the dizzy field by 1
*/
void 					decreaseDizzyness(zlist_t * list)
{
	player_t *p;
    p = zlist_first(list);
    if (p->dizzy > 0)
    	p->dizzy = p->dizzy - 1;
    while (p != NULL)
    {
        if (p->dizzy > 0)
    		p->dizzy = p->dizzy - 1;
        p = zlist_next(list);
    }
}

/*
* Return a power's energy then remove it from the list
*/
energycell_t 					*get_energy(zlist_t * list, int x, int y)
{
	energycell_t *e;
    e = zlist_first(list);
    if (e->value != EMPTY && (e->x == x && e->y == y))
    	return (e);
    while (e != NULL)
    {
        if (e->value != EMPTY && (e->x == x && e->y == y))
    		return (e);
        e = zlist_next(list);
    }
    return (NULL);
}

/*
*	return 0 or 1 if the move lead straight to the borderline of thge map
*/
int 					move(gameinfo_t *game, player_t *p, int moves)
{
	if (p->looking == LOOK_UP)
	{
		//can't move on another player
		if ((p->x - moves) > 0)
		{
			if (game->map->map[p->x - moves][p->y] != EMPTY && game->map->map[p->x - moves][p->y] != ENERGY)
				return 0;
			else
				p->x -= moves;
		}
	}
	else if (p->looking == LOOK_DOWN)
	{
		if ((p->x + moves) < game->map_size - 1)
		{
			if (game->map->map[p->x + moves][p->y] != EMPTY && game->map->map[p->x + moves][p->y] != ENERGY)
				return 0;
			else
				p->x += moves;
		}
	}
	else if (p->looking == LOOK_LEFT)
	{
		if ((p->y - moves) > 0)
		{
			if (game->map->map[p->x][p->y - moves] != EMPTY && game->map->map[p->x][p->y - moves] != ENERGY)
				return 0;
			else
				p->y -= moves;
		}
	}
	else if (p->looking == LOOK_RIGHT)
	{
		if ((p->y + moves) < game->map_size - 1)
		{
			if (game->map->map[p->x][p->y + moves] != EMPTY && game->map->map[p->x][p->y + moves] != ENERGY)
				return 0;
			else
				p->y += moves;
		}
	}
	return 1;
}

int 					back(gameinfo_t *game, player_t *p)
{
	if (p->looking == LOOK_UP)
	{
		//can't move on another player
		if ((p->x + 1) < game->map_size - 1)
		{
			if (game->map->map[p->x + 1][p->y] >= 0)
				return 0;
			else
				p->x += 1;
		}
	}
	else if (p->looking == LOOK_DOWN)
	{
		if ((p->x - 1) > 0)
		{
			if (game->map->map[p->x - 1][p->y] >= 0)
				return 0;
			else
				p->x -= 1;
		}
	}
	else if (p->looking == LOOK_LEFT)
	{
		if ((p->y + 1) < game->map_size - 1)
		{
			if (game->map->map[p->x][p->y + 1] >= 0)
				return 0;
			else
				p->y += 1;
		}
	}
	else if (p->looking == LOOK_RIGHT)
	{
		if ((p->y - 1) > 0)
		{
			if (game->map->map[p->x][p->y - 1] >= 0)
				return 0;
			else
				p->y -= 1;
		}
	}
	return 1;
}


/*
*	return 0 or 1 if an enemmy is within the vision's field
*/
int 					visionUp(gameinfo_t *game, int x, int y)
{
	player_t *p;
	int **map = game->map->map;
	if ((x - 1) > 0 && map[x - 1][y] >= 0)
	{
		p = get_playerByPos(game->players, x - 1, y);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((x - 2) > 0 && map[x - 2][y] >= 0)
	{
		p = get_playerByPos(game->players, x - 2, y);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((x - 2) > 0 && (y - 1) > 0 && map[x - 2][y - 1] >= 0)
	{
		p = get_playerByPos(game->players, x - 2, y - 1);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((x - 2) > 0 && (y + 1) < game->map_size - 1 && map[x - 2][y + 1] >= 0)
	{
		p = get_playerByPos(game->players, x - 2, y + 1);
		p->dizzy = DIZZY;
		return 1;
	}
	//no match happened, return false
	return 0;
}

int 					visionDown(gameinfo_t *game, int x, int y)
{
	player_t *p;
	int **map = game->map->map;
	if ((x + 1) < game->map_size - 1 && map[x + 1][y] >= 0)
	{
		p = get_playerByPos(game->players, x + 1, y);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((x + 2) < game->map_size - 1 && map[x + 2][y] >= 0)
	{
		p = get_playerByPos(game->players, x + 2, y);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((x + 2) < game->map_size - 1 && (y + 1) < game->map_size - 1 && map[x + 2][y + 1] >= 0)
	{
		p = get_playerByPos(game->players, x + 2, y + 1);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((x + 2) < game->map_size - 1 && (y - 1) > 0 && map[x + 2][y - 1] >= 0)
	{
		p = get_playerByPos(game->players, x + 2, y - 1);
		p->dizzy = DIZZY;
		return 1;
	}
	//no match happened, return false
	return 0;
}

int 					visionRight(gameinfo_t *game, int x, int y)
{
	player_t *p;
	int **map = game->map->map;
	if ((y + 1) < game->map_size - 1 && map[x][y + 1] >= 0)
	{
		p = get_playerByPos(game->players, x, y + 1);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((y + 2) < game->map_size - 1 && map[x][y + 2] >= 0)
	{
		p = get_playerByPos(game->players, x, y + 2);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((y + 2) < game->map_size - 1 && (x - 1) > 0 && map[x][y + 2] >= 0)
	{
		p = get_playerByPos(game->players, x - 1, y + 2);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((y + 2) < game->map_size - 1 && (x - 1) > 0 && map[x][y + 2] >= 0)
	{
		p = get_playerByPos(game->players, x + 1, y + 2);
		p->dizzy = DIZZY;
		return 1;
	}
	//no match happened, return false
	return 0;
}

int 					visionLeft(gameinfo_t *game, int x, int y)
{
	player_t *p;
	int **map = game->map->map;
	if ((y - 1) > 0 && map[x][y - 1] >= 0)
	{
		p = get_playerByPos(game->players, x, y - 1);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((y - 2) > 0 && map[x][y - 2] >= 0)
	{
		p = get_playerByPos(game->players, x, y - 2);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((y - 2) > 0 && (x - 1) > 0 && map[x - 1][y - 2] >= 0)
	{
		p = get_playerByPos(game->players, x - 1, y - 2);
		p->dizzy = DIZZY;
		return 1;
	}
	if ((y - 2) > 0 && (x + 1) < game->map_size - 1 && map[x + 1][y - 2] >= 0)
	{
		p = get_playerByPos(game->players, x + 1, y - 2);
		p->dizzy = DIZZY;
		return 1;
	}
	//no match happened, return false
	return 0;
}

int 					doAttack(gameinfo_t *game, player_t *p)
{
	if (visionUp(game, p->x, p->y))
		return 1;
	if (visionLeft(game, p->x, p->y))
		return 1;
	if (visionRight(game, p->x, p->y))
		return 1;
	if (visionDown(game, p->x, p->y))
		return 1;
	//no match happened, return false
	return 0;
}

char 					*format(int n)
{
	char *answer;
	answer = malloc(sizeof(char) * 50);
	if (n == ENERGY)
		sprintf(answer, "%s", "energy");
	if (n == EMPTY)
		sprintf(answer, "%s", "empty");
	else
		sprintf(answer, "#0x0%d", n);
	return answer;
}

/*
*	return 0 or 1 if an enemmy is within the vision's field
*/
char 					*getvisionUp(gameinfo_t *game, int x, int y)
{
	char *answer, *case1, *case2, *case3, *case4;
	int **map = game->map->map;
	answer = malloc(sizeof(char) * 50);
	case1 = malloc(sizeof(char) * 50);
	case2 = malloc(sizeof(char) * 50);
	case3 = malloc(sizeof(char) * 50);
	case4 = malloc(sizeof(char) * 50);
	if ((x - 1) >= 0)//case 1
		strcpy(case1, format(map[x-1][y]));
	if ((x - 2) >= 0)//case 3
		strcpy(case3, format(map[x-2][y]));
	if ((x - 2) >= 0 && (y - 1) > 0)//case 2
		strcpy(case2, format(map[x-2][y-1]));
	if ((x - 2) >= 0 && (y + 1) < game->map_size - 1)//case 4
		strcpy(case4, format(map[x-2][y+1]));
	sprintf(answer, "[%s,%s,%s,%s]", case1, case2, case3, case4);
	return answer;
}

char 					*getvisionDown(gameinfo_t *game, int x, int y)
{
	char *answer, *case1, *case2, *case3, *case4;
	int **map = game->map->map;
	answer = malloc(sizeof(char) * 50);
	case1 = malloc(sizeof(char) * 50);
	case2 = malloc(sizeof(char) * 50);
	case3 = malloc(sizeof(char) * 50);
	case4 = malloc(sizeof(char) * 50);
	if ((x + 1) < game->map_size - 1)
		strcpy(case1, format(map[x+1][y]));
	if ((x + 2) < game->map_size - 1)
		strcpy(case3, format(map[x+2][y]));
	if ((x + 2) < game->map_size - 1 && (y + 1) < game->map_size - 1)
		strcpy(case2, format(map[x+2][y+1]));
	if ((x + 2) < game->map_size - 1 && (y - 1) > 0)
		strcpy(case4, format(map[x+2][y-1]));
	sprintf(answer, "[%s,%s,%s,%s]", case1, case2, case3, case4);
	return answer;
}

char 					*getvisionRight(gameinfo_t *game, int x, int y)
{
	char *answer, *case1, *case2, *case3, *case4;
	int **map = game->map->map;
	answer = malloc(sizeof(char) * 50);
	case1 = malloc(sizeof(char) * 50);
	case2 = malloc(sizeof(char) * 50);
	case3 = malloc(sizeof(char) * 50);
	case4 = malloc(sizeof(char) * 50);
	if ((y + 1) < game->map_size - 1)
		strcpy(case1, format(map[x][y+1]));
	if ((y + 2) < game->map_size - 1)
		strcpy(case3, format(map[x][y+2]));
	if ((y + 2) < game->map_size - 1 && (x - 1) > 0)
		strcpy(case2, format(map[x-1][y+2]));
	if ((y + 2) < game->map_size - 1 && (x - 1) > 0)
		strcpy(case4, format(map[x+1][y+2]));
	sprintf(answer, "[%s,%s,%s,%s]", case1, case2, case3, case4);
	return answer;
}

char 					*getvisionLeft(gameinfo_t *game, int x, int y)
{
	char *answer, *case1, *case2, *case3, *case4;
	int **map = game->map->map;
	answer = malloc(sizeof(char) * 50);
	case1 = malloc(sizeof(char) * 50);
	case2 = malloc(sizeof(char) * 50);
	case3 = malloc(sizeof(char) * 50);
	case4 = malloc(sizeof(char) * 50);
	if ((y - 1) >= 0 && map[x][y - 1] >= 0)
		strcpy(case1, format(map[x][y-1]));
	if ((y - 2) >= 0 && map[x][y - 2] >= 0)
		strcpy(case3, format(map[x][y-2]));
	if ((y - 2) >= 0 && (x - 1) > 0 && map[x - 1][y - 2] >= 0)
		strcpy(case2, format(map[x-1][y-2]));
	if ((y - 2) >= 0 && (x + 1) < game->map_size - 1 && map[x + 1][y - 2] >= 0)
		strcpy(case4, format(map[x+1][y-2]));
	sprintf(answer, "[%s,%s,%s,%s]", case1, case2, case3, case4);
	return answer;
}

char 					*getVision(gameinfo_t *game, player_t *p)
{
	printf("looking:%u\n", p->looking);
	if (p->looking == LOOK_UP)
		return getvisionUp(game, p->x, p->y);
	if (p->looking == LOOK_DOWN)
		return getvisionDown(game, p->x, p->y);
	if (p->looking == LOOK_LEFT)
		return getvisionLeft(game, p->x, p->y);
	if (p->looking == LOOK_RIGHT)
		return getvisionRight(game, p->x, p->y);
	return "NULL";
}


