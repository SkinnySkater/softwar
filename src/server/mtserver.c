#include "zhelpers.h"
#include <pthread.h>
#include <unistd.h>
#include <czmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define SUBSCRIBERS_EXPECTED    2
#define SEPARATOR_CMD           "|"
#define SUCESSFULLY_REQ         "ok"
#define UNSUCESSFULLY_REQ       "ko"
#define SUS_CHANNEL             "UPDATE"

char *generate_answer(char *status, char* data)
{
    char *answer;
    answer = malloc(sizeof(char) * 50);

    sprintf(answer, "%s%s%s", status, SEPARATOR_CMD, data);
    return (answer);
}

int  check_zlist(zlist_t *list, char *item_)
{
    char *item;
    item = zlist_first(list);
    int i = 1;
    while (item != NULL)
    {
        printf("item[%d]: %s\n", i, item);
        if (!strcmp(item, item_))
            return 1;
        i++;
        item = zlist_next(list);
    }
    return 0;
}

void send_sus(void *publisher, char *msg)
{
    s_sendmore (publisher, SUS_CHANNEL);
    s_send (publisher, msg);
}

static void *publisher;
static int suscribers;
zlist_t *id_list;

void valid_user(void *receiver, char *name)
{
    int state = 1;
    if (check_zlist(id_list, name) == 0)
    {
        suscribers++;
        //add the new id into the generic list
        zlist_append(id_list, name);
        //reply the identify request
        s_send(receiver, generate_answer(SUCESSFULLY_REQ, name));
        state = 0;
    }
    s_send(receiver, generate_answer(UNSUCESSFULLY_REQ, ""));
    while (state != 0)
    {
        char *data;
        char *string = s_recv(receiver);
        printf ("#Received request: [%s]\n", string);
        data = strtok(string, SEPARATOR_CMD);
        data = strtok(NULL, SEPARATOR_CMD);
        printf("name:: %s\n", data);
        if (check_zlist(id_list, data) == 0)
        {
            suscribers++;
            //add the new id into the generic list
            zlist_append(id_list, data);
            //reply the identify request
            s_send(receiver, generate_answer(SUCESSFULLY_REQ, data));
            state = 0;
        }
        else
            s_send(receiver, generate_answer(UNSUCESSFULLY_REQ, ""));
        free(string);
        //free(data);
    }
    printf("Player %d is Waiting for all players to log in ...\n", suscribers);
    //wait every suscribers are logged in
    while (suscribers < SUBSCRIBERS_EXPECTED){
        send_sus(publisher, "wait");
        sleep (1);
    }
    send_sus(publisher, "ready");
    //Notify the begening of the game
    send_sus(publisher, "Starting The Game.");
}

static void * worker_routine(void *context) {
    //  Socket to talk to dispatcher
    void *receiver = zmq_socket (context, ZMQ_REP);
    int state = 1;
    zmq_connect (receiver, "inproc://workers");
    // check if the client comes too late
    printf("suscribers: %d\n", suscribers);
    if (suscribers >= SUBSCRIBERS_EXPECTED)
    {
        s_send(receiver, generate_answer(UNSUCESSFULLY_REQ, "game full"));
        zmq_close(receiver);
        return (NULL);
    }
    while (1)
    {
        char *data;
        char *string = s_recv (receiver);
        char *tmp;
        printf ("#Received request: [%s]\n", string);
        tmp = strdup(string);
        data = strtok(tmp, SEPARATOR_CMD);
        if (!strcmp(data, "identify")){
            char *name = strtok(NULL, SEPARATOR_CMD);
            printf("name->%s\n", name);
            valid_user(receiver, name);
            //free(name);
        }
        free(string);
        free(data);
        //  Do some 'work'
        sleep (1);
        //  Send reply back to client
        s_send (receiver, "World");
    }
    zmq_close (receiver);
    return NULL;
}


int main (void)
{
    void *context = zmq_ctx_new();
    suscribers = 0;
    id_list = zlist_new();
    //  Socket to talk to clients
    void *clients = zmq_socket (context, ZMQ_ROUTER);
    zmq_bind (clients, "tcp://*:5555");

    //  Socket to talk to workers
    void *workers = zmq_socket (context, ZMQ_DEALER);
    zmq_bind (workers, "inproc://workers");

    //  Prepare our context and publisher
    publisher = zmq_socket (context, ZMQ_PUB);
    zmq_bind (publisher, "tcp://*:5563");

    //  Launch pool of worker threads
    int thread_nbr;
    for (thread_nbr = 0; thread_nbr < 2; thread_nbr++) {
        pthread_t worker;
        pthread_create (&worker, NULL, worker_routine, context);
    }
    //  Connect work threads to client threads via a queue proxy
    zmq_proxy (clients, workers, NULL);

    //  We never get here, but clean up anyhow
    zmq_close (clients);
    zmq_close (workers);
    zmq_close (publisher);
    zmq_ctx_destroy (context);
    return 0;
}
