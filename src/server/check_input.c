#include "notification.h"

int						char_isnum(char s)
{
  return (s > 47 && s < 58);
}

int						is_nbr(char *str)
{
  int	i;
  int	len;

  len = strlen(str);
  i = 0;
  if (str[i] != '-' && !char_isnum(str[i]))
    return (i == len);
  while (i < len && char_isnum(str[i]))
    i++;
  return (i == len);
}

void p_usage()
{
	printf("%s\n", "–rep-port [port utilisé pour recevoir les commandes des clients et répondre]");
	printf("%s\n", "–pub-port [port utilisé pour envoyer les notifications aux clients]");
	printf("%s\n", "–cycle [nombre en microsecondes correspondant à un cycle (> 0)]");
	printf("%s\n", "-v (active le mode verbose (loglevel INFO))");
	printf("%s\n", "–log [fichier de log]");
	printf("%s\n", "–size [taille de la map]");
	printf("Example:\n\t\t%s\n", "./server -v --size 8 --log /tmp/soft_war.log --cycle 10000 --rep-port 5555 --pub-port 5561");
}

s_config                 *init_config()
{
    s_config             *config;

    config = malloc(sizeof * config);
    if (!config)
    {
    	fprintf(stderr, "ERROR : malloc disfunction on config init.\n");
    	exit(EXIT_FAILURE);
    }
    config->log = malloc(MAX_IN);
    if (!config->log)
    {
    	fprintf(stderr, "ERROR : malloc disfunction on log init.\n");
    	exit(EXIT_FAILURE);
    }
    config->rep_port = malloc(sizeof(char) * 10);
    config->pub_port = malloc(sizeof(char) * 10);
    strcpy(config->rep_port, DEFAULT_RPORT);
    strcpy(config->pub_port, DEFAULT_PPORT);
    config->cycle 	 = DEFAULT_CYCLE;
    config->verbose  = FALSE;
    strcpy(config->log, DEFAULT_LOG);
    config->size 	 = MIN_MAP_SIZE;

    return (config);
}

int 					is_in_port_range(int n)
{
	return (n >= 0 && n <= MAX_PORT);
}

int 					store_log_file(char *fname)
{
	FILE *fp;

	fp = fopen(fname, "a+");
	if (fp == NULL) {
    	perror("Error: ");
    	return(FALSE);
    }
	fclose(fp);

	return(TRUE);
}