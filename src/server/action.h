#ifndef		_ACTION_H_
# define	_ACTION_H_

#include "notification.h"

/* ACTIONS COMMAND's MACRO */
#define IDENTIFY			"identify"
#define FORWARD 			"forward"
#define BACKWARD			"backward"
#define LEFTWD				"leftwd"
#define RIGHTWD				"rightwd"
#define RIGHT				"right"
#define LEFT				"left"
#define LOOKING				"looking"
#define GATHER 				"gather"
#define WATCH 				"watch"
#define ATTACK 				"attack"
#define SELFSTAT			"selfstat"
#define INSPECT				"inspect"
#define NEXT				"next"
#define JUMP				"jump"

/* ACTIONS POINT'S MACRO */
#define ACTION_PTS_INS		0.5
#define ACTION_PTS_GATHER	0.5
#define ACTION_PTS_FORWARD	1
#define ACTION_PTS_ATTACK	0.5
#define ACTION_PTS_RIGHT	0.5
#define ACTION_PTS_LEFT		0.5
#define ACTION_PTS_LEFTFWD	1
#define ACTION_PTS_RIGHTFWD	1

char						*action(gameinfo_t *game, char *string, void *publisher);

#endif		/* !_ACTION_H_*/