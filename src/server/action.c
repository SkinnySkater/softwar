#include "action.h"



/*
 * Examiner un processus à partir de sa signature numérique
 * withdrew 0.5 action point
*/
char						*inspect(zlist_t * player, char *name)
{
	player_t *p;
	char *num = malloc(sizeof(char) * 50);

	p = get_player(player, name);
	if (p == NULL || p->dizzy > 0 || p->action < ACTION_PTS_INS)
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	else{
		p->action -= ACTION_PTS_INS;
		sprintf(num, "%d", (int)p->energy);
		return (generate_answer(SUCESSFULLY_REQ, num));
	}
}

/*
 *  Le processus demande la fin de son tour
*/
char						*next()
{
	return (generate_answer(SUCESSFULLY_REQ, ""));
}

/*
* Return OK  and the coordinate of the if the player 
* is sit on energy or see it in front of him
* Return Null otherwise
* Then Push notification of the gathered energy to make the 
* players grapic part remove the energy balls
* FORMAT: EL|x:y
*/
char						*gather(gameinfo_t *game, char *name, void *publisher)
{
	char *answer;
	player_t *p;
	energycell_t *ec;
	int total_e;

	p = get_player(game->players, name);
	ec = get_energy(game->energy_cells, p->x, p->y);
	if (p == NULL || p->dizzy > 0 || p->action < ACTION_PTS_GATHER
	 || game->map->map[p->x][p->y] != ENERGY)
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	else if (game->map->map[p->x][p->y] == ENERGY)
	{
		p->action -= ACTION_PTS_GATHER;
		total_e = p->energy + ec->value;
		if (total_e > MAX_ENERGY)
			p->energy -= (total_e - MAX_ENERGY);
		else
			p->energy = total_e;
		//send  data to let other client remove the graphical energy ball
		ec->value = EMPTY;
	    answer = malloc(sizeof(char) * 50);
	    sprintf(answer, "%s%s%u%s%u", PUB_ENRGY_LOST, SEPARATOR_CMD, ec->x, ":", ec->y);
	    s_send (publisher, answer);
		return (generate_answer(SUCESSFULLY_REQ, ""));
	}
}


char						*forwardOrJump(gameinfo_t *game, char *name, int moves)
{
	player_t *p;

	p = get_player(game->players, name);
	if (moves == 1 && p->action < ACTION_PTS_FORWARD)
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	if (p == NULL || p->dizzy > 0 || !move(game, p, moves))
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	//if Jump is received then no action point will be widrawn but 2 energy point will
	if (moves == 2)
		p->energy -= moves;
	else
		p->action -= ACTION_PTS_FORWARD;
	return (generate_answer(SUCESSFULLY_REQ, ""));

}

char						*attack(gameinfo_t *game, char *name)
{
	player_t *p;

	p = get_player(game->players, name);
	if (p == NULL || p->dizzy > 0 || p->action < ACTION_PTS_ATTACK || !doAttack(game, p))
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	p->action -= ACTION_PTS_ATTACK;
	p->energy -= 2;
	return (generate_answer(SUCESSFULLY_REQ, ""));
}

/*
*  Orientation 	La direction dans laquelle le processus regarde
*/
char						*looking(gameinfo_t *game, char *name)
{
	player_t *p;
	char *answer;
	p = get_player(game->players, name);
	if (p == NULL || p->dizzy > 0)
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	answer = malloc(sizeof(char) * 50);
	sprintf(answer, "%u", p->looking);
	return (generate_answer(SUCESSFULLY_REQ, answer));
}


/*
*	Le processus recule d’une case
*/
char						*backward(gameinfo_t *game, char *name)
{
	player_t *p;

	p = get_player(game->players, name);
	if (p->action < ACTION_PTS_FORWARD)
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	if (p == NULL || p->dizzy > 0 || !back(game, p))
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	p->action -= ACTION_PTS_FORWARD;
	return (generate_answer(SUCESSFULLY_REQ, ""));
}

/*
*	Pivoter vers la droite
*/
char						*right(gameinfo_t *game, char *name)
{
	player_t *p;

	p = get_player(game->players, name);
	if (p == NULL || p->action < ACTION_PTS_RIGHT)
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	if (p->looking + 1 > 3)
		p->looking = LOOK_UP;
	else
		p->looking += 1;
	p->action -= ACTION_PTS_RIGHT;
	return (generate_answer(SUCESSFULLY_REQ, ""));
}

/*
*	Pivoter vers la left
*/
char						*left(gameinfo_t *game, char *name)
{
	player_t *p;

	p = get_player(game->players, name);
	if (p == NULL || p->action < ACTION_PTS_LEFT)
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	if (p->looking - 1 < 0)
		p->looking = LOOK_UP;
	else
		p->looking -= 1;
	p->action -= ACTION_PTS_LEFT;
	return (generate_answer(SUCESSFULLY_REQ, ""));
}

/*
*	Le processus regarde les cases devant lui:
* 	Answer : ["case1", "case2", "case3", "case4"]
*/
char						*watch(gameinfo_t *game, char *name)
{
	player_t *p;

	p = get_player(game->players, name);
	if (p == NULL)
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	return (generate_answer(SUCESSFULLY_REQ, getVision(game, p)));
}


/*
*	Le processus pivote vers la gauche et avance d’une case
*/
char						*leftfwd(gameinfo_t *game, char *name)
{
	player_t *p;
	const char isOk[3] = "ok";

	p = get_player(game->players, name);
	if (p == NULL || p->action < ACTION_PTS_LEFTFWD
	 || !strstr(left(game, name), isOk)
	 || !strstr(forwardOrJump(game, name, 1), isOk))
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	return (generate_answer(SUCESSFULLY_REQ, ""));
}


/*
*	Le processus pivote vers la gauche et avance d’une case
*/
char						*rightfwd(gameinfo_t *game, char *name)
{
	player_t *p;
	const char isOk[3] = "ok";

	p = get_player(game->players, name);
	if (p == NULL || p->action < ACTION_PTS_LEFTFWD
	 || !strstr(right(game, name), isOk)
	 || !strstr(forwardOrJump(game, name, 1), isOk))
		return (generate_answer(UNSUCESSFULLY_REQ, ""));
	return (generate_answer(SUCESSFULLY_REQ, ""));
}

/*
*	Le processus envoie une instruction a executer
*	Receive: ok/ko|data
*/
char						*action(gameinfo_t *game, char *string, void *publisher)
{
	player_t *p;
	char *name;
	char *cmd;

	cmd = get_cmd_from_client(strdup(string));
	name = get_arg_from_client(strdup(string));
	p = get_player(game->players, name);
	if (p == NULL)
		return generate_answer(UNSUCESSFULLY_REQ, "");
	printf("%s has %u energy\n", p->name, p->energy);
	if (!p->energy){
		generate_reportInfoDead(game, CLIENT_DEAD, p->name, publisher);
		return generate_answer(UNSUCESSFULLY_REQ, "");
	}
	//check if dead or dizzy
	if (p->dizzy > 0 || p->energy <= 0)
		return generate_answer(UNSUCESSFULLY_REQ, "");
	if (!strcmp(cmd, FORWARD))
		return forwardOrJump(game, name, 1);
	if (!strcmp(cmd, BACKWARD))
		return backward(game, name);
	if (!strcmp(cmd, LEFTWD))
		return leftfwd(game, name);
	if (!strcmp(cmd, RIGHTWD))
		return rightfwd(game, name);
	if (!strcmp(cmd, RIGHT))
		return right(game, name);
	if (!strcmp(cmd, LEFT))
		return left(game, name);
	if (!strcmp(cmd, LOOKING))
		return looking(game, name);
	if (!strcmp(cmd, GATHER))
		return gather(game, name, publisher);
	if (!strcmp(cmd, WATCH))
		return watch(game, name);
	if (!strcmp(cmd, ATTACK))
		return attack(game, name);
	if (!strcmp(cmd, INSPECT))
		return inspect(game->players, name);
	if (!strcmp(cmd, NEXT))
		return next();
	if (!strcmp(cmd, JUMP))
		return forwardOrJump(game, name, 2);
	return NULL;
}