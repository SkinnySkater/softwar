#ifndef		_NOTIFICATION_H_
# define	_NOTIFICATION_H_

#include <czmq.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>

#include "zhelpers.h"

#define TRUE            1
#define FALSE           0
/* option filtering's maccro */
#define MAX_IN 	      	64
#define MAX_PORT        65535
#define MIN_MAP_SIZE    5
#define DEFAULT_CYCLE   500000 // 1/2 second
#define DEFAULT_RPORT   "4242"
#define DEFAULT_PPORT   "4243"
#define DEFAULT_LOG     "/tmp/softwar/sw.log"

//#define randof(num) (int) ((float) (num) * random () / (RAND_MAX + 1.0))
#define randofm(min, max) (int) ( random() % (max + 1 - min) + min)

/* INITIALIZE SERVER's MACRO */
#define SUBSCRIBERS_EXPECTED    4
#define SEPARATOR_CMD           "|"
#define SUCESSFULLY_REQ         "ok"
#define UNSUCESSFULLY_REQ       "ko"
#define SUS_CHANNEL             "UPDATE"
#define GAME_STATE_ON            1
#define GAME_STATE_OFF           0

/* NOTIFICATION's MACRO */
#define CYCLE_INFO			0
#define GAME_STARTED 		1
#define GAME_FINISHED		2
#define CLIENT_DEAD			3
#define CLIENT_WIN			4
/* GAME STATUS's MACRO */
#define WAITING				0
#define STARTED 			1
#define FINISHED			2
/* DIRECTION'S MACRO */
#define LOOK_UP				0
#define LOOK_RIGHT	 		1
#define LOOK_DOWN			2
#define LOOK_LEFT			3

/* ENERGY'S MACRO */
#define DEFAULT_ENERGY		50
#define MAX_ENERGY			100

/* MAP'S MACRO */
#define ENERGY				-2
#define EMPTY				-1

#define DIZZY				2

#define PUB_ENRGY_LOST 		"EL"
#define PUB_ENRGY_CREATED 	"EC"

#define PUB_PLAYER_DEAD 	"PD"
#define PUB_PLAYER_CREATED 	"PC"

/* ENERGY CELL's structure */
typedef struct {
	unsigned int 			x;
	unsigned int 			y;
	unsigned int 	 		value;
} energycell_t;

/* PLAYER's structure */
typedef struct {
	char					*name;
	int 					dizzy;
	unsigned int 			x;
	unsigned int 			y;
	unsigned int 	 		energy;
	unsigned int  			looking;
	unsigned int 			action;
} player_t;

/* map's structure */
typedef struct {
  int      **map;
} map_t;

typedef struct {
  char*       rep_port;
  char*       pub_port;
  int         cycle;
  int         verbose;  
  char        *log;
  int         size;
} s_config;

/* Game Information's structure */
typedef struct {
	unsigned int 			map_size;
	unsigned int 			game_status;
	zlist_t * 				players;
	zlist_t *  				energy_cells;
	map_t	*				map;
	s_config * 				config;
} gameinfo_t;

/* FUNCTIONS CONFIG */
void        				p_usage();
s_config    				*init_config();
int         				is_nbr(char *str);
int         				is_in_port_range(int n);
int         				store_log_file(char *fname);

/* main server class */

int 						run(s_config *conf);

void                        add_player(gameinfo_t *gi, char *id, void *publisher);
gameinfo_t                  *init_gameInfo(gameinfo_t *gi, unsigned int map_size);
void                        generate_reportInfo(gameinfo_t *gi, int type, void *publisher);

void						generate_energycell(gameinfo_t *gi, void *publisher);
void 						decreaseDizzyness(zlist_t * players);

/* UTILS METHODS */
player_t 					*get_player(zlist_t * list, char *name);
player_t 					*get_playerByPos(zlist_t * list, int x , int y);
energycell_t 				*get_energy(zlist_t * list, int x, int y);

char 						*generate_answer(char *status, char* data);
char 						*get_cmd_from_client(char *string);
char 						*get_arg_from_client(char *string);
int 						move(gameinfo_t *game, player_t *p, int moves);
int 						doAttack(gameinfo_t *game, player_t *p);
int 						back(gameinfo_t *game, player_t *p);
char 						*getVision(gameinfo_t *game, player_t *p);

void                        decrease_energy(zlist_t *list, void *publisher);
int 						check_winner(zlist_t * list);

void                        generate_reportInfoDead(gameinfo_t *gi, int type, char *name, void *publisher);

#endif		/* !_NOTIFICATION_H_*/