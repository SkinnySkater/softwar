#include "zhelpers.h"
#include <pthread.h>
#include <unistd.h>
#include <czmq.h>
#include <stdio.h>
#include "action.h"


static int game_state;
static void *publisher;
static void *syncservice;
static gameinfo_t *gi;
zlist_t *id_list;
static int i; 

void *cycle(void * config) {
    while (1){
        if (game_state == GAME_STATE_ON){
            if ((((s_config *)config)->verbose))
                printf("Cycle-%d\n", ((s_config *)config)->cycle);
            if ((((s_config *)config)->verbose))
                printf("New Cycle: %d\n", i);
            generate_energycell(gi, publisher);
            decreaseDizzyness(gi->players);
            decrease_energy(gi->players, publisher);
            usleep(((s_config *)config)->cycle * 100);
            generate_reportInfo(gi, CYCLE_INFO, publisher);
            //sleep(1);
        }
    }
}


static void *worker_routine (void *context) {
    //  Socket to talk to dispatcher
    if (gi->config->verbose)
        printf("thread on\n");
    void *receiver = zmq_socket (context, ZMQ_REP);
    zmq_connect (receiver, "inproc://workers");
    //int i = 0;
    while (1) {
        if (game_state == GAME_STATE_ON)
        {
            char *string = s_recv(receiver);
            printf("#Received request: [%s]\n", string);
            //  Do some 'work'
            //check if there is a winner here
            if (check_winner(gi->players))
                generate_reportInfo(gi, GAME_FINISHED, publisher);
            sleep(1);
            //usleep(gi->config->cycle * 10000);
            //s_send (receiver, "LOL");
            //  Send reply back to client
            s_send (receiver, action(gi, string, publisher));
            i++;
        }
    }
    zmq_close (receiver);
    return NULL;
}

static void *create_publisher(void * context)
{
    //  Socket to talk to clients
    publisher = zmq_socket (context, ZMQ_PUB);

    int sndhwm = 1100000;
    zmq_setsockopt (publisher, ZMQ_SNDHWM, &sndhwm, sizeof (int));

    char addr[50], port[50];
    strcpy(addr,  "tcp://*:");
    strcpy(port,  gi->config->pub_port);
    strcat(addr, port);
    zmq_bind (publisher, addr);
    if (gi->config->verbose)
        printf("Bind Pub socket to %s\n", addr);

    //  Socket to receive signals
    syncservice = zmq_socket (context, ZMQ_REP);
    zmq_bind (syncservice, "tcp://*:5562");

    //  Get synchronization from subscribers
    printf ("Waiting for subscribers to connect...\n");
    int subscriber = 0;
    char msg[50];
    while (1) {
            //  - wait for synchronization request
            char *data;
            char *string = s_recv(syncservice);
            data = get_cmd_from_client(strdup(string));
            if (!strcmp(data, "identify"))
            {
                if (subscriber < SUBSCRIBERS_EXPECTED)
                {
                    data = get_arg_from_client(strdup(string));
                    if (zlist_exists(id_list, strdup(data)) == 0)
                    {
                        subscriber++;
                        //add the new id into the generic list
                        add_player(gi, strdup(data), publisher);
                        zlist_append(id_list, strdup(data));
                        //reply to the identity request
                        sprintf(msg, "[%s]\n", generate_answer(SUCESSFULLY_REQ, data));
                        s_send(syncservice, msg);
                        printf("Subscriber[%d]:%s is logged in\n", subscriber, data);
                        if (subscriber >= SUBSCRIBERS_EXPECTED)
                        {
                            game_state = GAME_STATE_ON;
                            generate_reportInfo(gi, GAME_STARTED, publisher);
                            //s_send (publisher, "All suscribers are connected ! Run the Game.");
                        }
                    }
                    else
                        s_send(syncservice, generate_answer(UNSUCESSFULLY_REQ, ""));
                }
                else
                    s_send(syncservice, generate_answer(UNSUCESSFULLY_REQ, "game full"));
            }
    }
    return NULL;
}


int run(s_config *conf)
{
    i = 0;
    if (conf->verbose)
        printf("%s\n", "START SERVER");
    //Init Game structure
    gi = malloc(sizeof(gameinfo_t *));
    gi = init_gameInfo(gi, conf->size);
    gi->config = conf;
    gi->config->cycle = conf->cycle;
    gi->config->verbose = conf->verbose;
    game_state = GAME_STATE_OFF;
    if (conf->verbose)
        printf("%s - %s - cycle:%d - %d - %d - %s\n", conf->rep_port, conf->pub_port
            , conf->cycle, conf->verbose, conf->size, conf->log);
    
    void *context = zmq_ctx_new ();
    syncservice = NULL;
    id_list = zlist_new();

    //  Socket to talk to clients
    char addr[50], port[50];
    strcpy(addr,  "tcp://*:");
    strcpy(port,  gi->config->rep_port);
    strcat(addr, port);
    if (gi->config->verbose)
        printf("Connect socket to %s\n", addr);
    void *clients = zmq_socket (context, ZMQ_ROUTER);
    zmq_bind (clients, addr);

    //  Socket to talk to workers
    void *workers = zmq_socket (context, ZMQ_DEALER);
    zmq_bind (workers, "inproc://workers");

    //  Prepare our context and publisher
    pthread_t worker;
    pthread_create (&worker, NULL, create_publisher, context);

    pthread_t timer;
    pthread_create (&timer, NULL, cycle, conf);
    //  Launch pool of worker threads
    int thread_nbr;
    for (thread_nbr = 0; thread_nbr < 4; thread_nbr++) {
        pthread_t worker;
        pthread_create (&worker, NULL, worker_routine, context);
    }
    //  Connect work threads to client threads via a queue proxy
    zmq_proxy (clients, workers, NULL);

    //  We never get here, but clean up anyhow
    zmq_close (clients);
    zmq_close (workers);
    zmq_close (publisher);
    zmq_close (syncservice);
    zmq_ctx_destroy (context);
    return 0;
}
