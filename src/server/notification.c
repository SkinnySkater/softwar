#include "notification.h"

/*
*   INITIALIZE PLAYER
*   PUBLISH: PC|name|x:y
*/
void                        add_player(gameinfo_t *gi, char *id, void *publisher)
{
    char *answer;
    int nb = id[4] - '0';
    player_t *player;
    player = malloc(sizeof(player_t *));
    player->name = malloc(sizeof(char *) * 10);
    player->energy = DEFAULT_ENERGY;
    player->action = 1;
    player->dizzy = 0;
    strcpy(player->name, id);
    if (nb == 0){
        player->x = 0;
        player->y = 0;
        player->looking = LOOK_DOWN;
    }
    if (nb == 1){
        player->x = 0;
        player->y = gi->map_size - 1;
        player->looking = LOOK_DOWN;
    }
    if (nb == 2){
        player->x = gi->map_size - 1;
        player->y = 0;
        player->looking = LOOK_UP;
    }
    if (nb == 3){
        player->x = gi->map_size - 1;
        player->y = gi->map_size - 1;
        player->looking = LOOK_UP;
    }
    if (gi->config->verbose)
        printf("Player %s Created with number %d , x:%uy:%u\n", player->name, nb, player->x, player->y);
    gi->map->map[player->x][player->y] = nb;
    //ADD THE NEWLY CREATED PLAYER
    zlist_append(gi->players, player);
    //PUBLISH THE PLAYER IS ADDED
    answer = malloc(sizeof(char) * 50);
    sprintf(answer, "%s%s%s%s%u%s%u", PUB_PLAYER_CREATED, SEPARATOR_CMD
        , player->name, SEPARATOR_CMD, player->x, ":", player->y);
    if (gi->config->verbose)
        printf("Generate: %s\n", answer);
    s_send(publisher, answer);
}

map_t                       *init_map(unsigned int size)
{
    map_t *map;
    int **m, i, j, k;

    map = malloc(sizeof(map_t *));
    m = (int **)malloc(size * sizeof(int *));
    for (i = 0; i < size; i++)
        *(m + i) = (int *)malloc(size * sizeof(int));
    for (j = 0; j < size; j++)
        for (k = 0; k < size; k++)
            m[j][k] = EMPTY;
    map->map = m;
    return map;
}

gameinfo_t                        *init_gameInfo(gameinfo_t *gi, unsigned int map_size)
{
    gi->map_size = map_size;
    gi->game_status = WAITING;
    gi->players = zlist_new();
    gi->energy_cells = zlist_new();
    gi->map = init_map(map_size);
    gi->config = init_config();
    return gi;
}

/*
* 
*/
void                        generate_energycell(gameinfo_t *gi, void *publisher)
{
    printf("%s\n", "dans la fonctions");
    char *pub, *answer;
    int x, y, b, v, max;
    max = 5;
    energycell_t *energy;

    b = 0;
    energy = malloc(sizeof(energycell_t *));
    energy->value = randofm(5, 15);
    while (!b)
    {
        if (max == 0){
            printf("Abandon EC\n");
            return;
        }
        x = randoff(gi->map_size - 1);
        y = randoff(gi->map_size - 1);
        if (gi->map->map[x][y] == EMPTY){
            gi->map->map[x][y] = ENERGY;
            b = 1;
        }
        max--;
    }
    energy->x = x;
    energy->y = y;
    //ADD THE NEWLY CREATED ENERERGY CELL
    zlist_append(gi->energy_cells, energy);
    //PUBLISH THE ENERGY IS CREATED
    answer = malloc(sizeof(char) * 50);
    sprintf(answer, "%s%s%d%s%d", PUB_ENRGY_CREATED, SEPARATOR_CMD, energy->x, ":", energy->y);
    printf("Cell Generated:%s\n", answer);
    s_send(publisher, answer);
}

void                        generate_reportInfoDead(gameinfo_t *gi, int type, char *name, void *publisher)
{
    char str[4064];

    sprintf(str, "notification_type=%d|data=%s", type, name);
    s_send(publisher, str);
}

void                        generate_reportInfo(gameinfo_t *gi, int type, void *publisher)
{
    char str[4064];

    if (type == CYCLE_INFO)
    {
        sprintf(str, "notification_type=%d|data=map_size:%u\ngame_status:%u\n"
            , type, gi->map_size, gi->game_status);
    }
    else
        sprintf(str, "notification_type=%d|data=", type);
    s_send(publisher, str);
}

/*
* WITHDRAW 2 POINTS AT EVERY CYCLES
* PUBLISH THE DEATH IF A PLAYER IS DEAD
* FORMAT: PD|name
*/
void                        decrease_energy(zlist_t *list, void *publisher)
{
    player_t *p;
    int lose_energy = 2;
    char *pub;
    p = zlist_first(list);
    printf("ddecrease-------%u\n", p->energy);
    if (p->energy > 1){
        printf("%s has lost 1 unit energy\n", p->name);
        p->energy -= lose_energy;
    }
    else//THE PLAYER IS GOING TO DIE
    {
        p->energy = 0;
        pub = malloc(sizeof(char) * 50);
        sprintf(pub, "%s%s%s", PUB_PLAYER_DEAD, SEPARATOR_CMD, p->name);
        s_send (publisher, pub);
    }
    p = zlist_next(list);
    while (p != NULL) 
    {
        if (p->energy > 1){
            p->energy -= lose_energy;
            printf("%s has lost 1 unit energy\n", p->name);
        }
        else//THE PLAYER IS GOING TO DIE
        {
            p->energy = 0;
            pub = malloc(sizeof(char) * 50);
            sprintf(pub, "%s%s%s", PUB_PLAYER_DEAD, SEPARATOR_CMD, p->name);
            s_send (publisher, pub);
        }
        p = zlist_next(list);
    }
}