# Changelog
All notable changes to this project will be documented in this file.

## [0.6.11] - 2018-08-28
### Fixed
-  A lot

## [0.6.10] - 2018-08-27
### Added
-   Damages
-   Dead status

### Fixed
-   Image format (gif to png)
-   Few bugs

## [0.6.5] - 2018-08-26
### Fixed
-   Reduce number of energy balls thrown by the AI
-   Limite the map size in comparision to the window's size
-   Hide the mouse when she's above the window

## [0.6.2] - 2018-08-25
### Added
-   Attack mode for AI
-   Status above AI's head

### Fixed
-   AI's moves
-   Items out of map


## [0.5.3] - xxxx-xx-xx
### Added
-   Energy balls
    -   Sprites
    -   Method
    -   Visual effect

### Fixed
-   Limitation area for energy balls
-   Structure files
-   Sentences correction in README and launcher files

## [0.4.2] - xxxx-xx-xx
### Added
-   Jump move
    -   Method
-   pep8 followed
-   Matrix background

### Fixed
-   Matrix background slowed down
-   AI moves are improved but need more

## [0.3.1] - xxxx-xx-xx
### Fixed
-   Area limitations


## [0.3.1] - xxxx-xx-xx
### Added
-   AI
-   Area position
-   Area floor

### Fixed
-   AI moves slowed down

## [0.2.0] - xxxx-xx-xx
### Added
-   Loop for the menu
-   Menu interactions
    -   Selection
    -   Quit
    -   Credits


## [0.1.0] - xxxxx-xx-xx
### Added
-   Loop of the game
-   Player sprites , mouvements
