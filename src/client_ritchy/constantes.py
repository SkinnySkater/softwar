import pygame
import random

# *Taille de la fenetre
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_SIZE = (SCREEN_WIDTH, SCREEN_HEIGHT)

# *Taille de la map
SIZE_MAP = 10
if SIZE_MAP < 5:
    SIZE_MAP = 5

# *Les raccourcies pour les couleurs
WHITE = (255, 255, 255)
LIGHTGREY = (192, 192, 192)
DARKGREY = (128, 128, 128)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
GREEN2 = (154, 255, 0)
GREEN3 = (0, 255, 154)
YELLOW = (255, 255, 0)


# !##################################
# !       DEBUT RECTANGLE MENU      #
# !##################################
# *Taille du rectangle dans le menu(width et height)
RECTANGLE_WIDTH = 600
RECTANGLE_HEIGHT = 270

# *Positon X et Y du rectangle
POSITION_X_RECTANGLE = (SCREEN_WIDTH / 2) - (RECTANGLE_WIDTH / 2)
POSITION_Y_RECTANGLE = (SCREEN_HEIGHT / 2) - (RECTANGLE_HEIGHT / 2)

# *Fond du texte du menu
RECTANGLE_FOND_TEXTE_MENU = pygame.Rect(POSITION_X_RECTANGLE,
                                        POSITION_Y_RECTANGLE,
                                        RECTANGLE_WIDTH,
                                        RECTANGLE_HEIGHT)

# *Police du titre du menu
TEXT_FONT_TITLE_MENU = './font/Pricedown title menu.ttf'

# *Police des modes du menu
TEXT_FONT_MODS_MENU = './font/menu mode.TTF'
# !##################################
# !       FIN RECTANGLE MENU        #
# !##################################


# !##################################
# !           DEBUT IMAGES          #
# !##################################
# *Image de l'icone de la barre de tache
IMAGE_ICON = './images/icon/glovesicon.png'
IMAGE_ICON = pygame.image.load(IMAGE_ICON)

# *Taille du sprite
SPRITE_SIZE = 50

# *Images du mage
MAGE = [
    './images/sprites/characters/mage/mage_up1.png',
    './images/sprites/characters/mage/mage_up2.png',
    './images/sprites/characters/mage/mage_right1.png',
    './images/sprites/characters/mage/mage_right2.png',
    './images/sprites/characters/mage/mage_down1.png',
    './images/sprites/characters/mage/mage_down2.png',
    './images/sprites/characters/mage/mage_left1.png',
    './images/sprites/characters/mage/mage_left2.png'
    ]

# *Images d'adam
ADAM = [
    './images/sprites/characters/adam/adam_up1.png',
    './images/sprites/characters/adam/adam_up2.png',
    './images/sprites/characters/adam/adam_right1.png',
    './images/sprites/characters/adam/adam_right2.png',
    './images/sprites/characters/adam/adam_down1.png',
    './images/sprites/characters/adam/adam_down2.png',
    './images/sprites/characters/adam/adam_left1.png',
    './images/sprites/characters/adam/adam_left2.png'
    ]

# *Images de bill
BILL = [
    './images/sprites/characters/bill/bill_up1.png',
    './images/sprites/characters/bill/bill_up2.png',
    './images/sprites/characters/bill/bill_right1.png',
    './images/sprites/characters/bill/bill_right2.png',
    './images/sprites/characters/bill/bill_down1.png',
    './images/sprites/characters/bill/bill_down2.png',
    './images/sprites/characters/bill/bill_left1.png',
    './images/sprites/characters/bill/bill_left2.png'
    ]

# *Images de ninja
NINJA = [
    './images/sprites/characters/ninja/ninja1_up1.png',
    './images/sprites/characters/ninja/ninja1_up2.png',
    './images/sprites/characters/ninja/ninja1_right1.png',
    './images/sprites/characters/ninja/ninja1_right2.png',
    './images/sprites/characters/ninja/ninja1_down1.png',
    './images/sprites/characters/ninja/ninja1_down2.png',
    './images/sprites/characters/ninja/ninja1_left1.png',
    './images/sprites/characters/ninja/ninja1_left2.png'
    ]

# *Images de knight
KNIGHT = [
    './images/sprites/characters/knight/knight_up1.png',
    './images/sprites/characters/knight/knight_up2.png',
    './images/sprites/characters/knight/knight_right1.png',
    './images/sprites/characters/knight/knight_right2.png',
    './images/sprites/characters/knight/knight_down1.png',
    './images/sprites/characters/knight/knight_down2.png',
    './images/sprites/characters/knight/knight_left1.png',
    './images/sprites/characters/knight/knight_left2.png'
]

# *Images de princess
PRINCESS = [
    './images/sprites/characters/princess/princess_up1.png',
    './images/sprites/characters/princess/princess_up2.png',
    './images/sprites/characters/princess/princess_right1.png',
    './images/sprites/characters/princess/princess_right2.png',
    './images/sprites/characters/princess/princess_down1.png',
    './images/sprites/characters/princess/princess_down2.png',
    './images/sprites/characters/princess/princess_left1.png',
    './images/sprites/characters/princess/princess_left2.png'
]

# *Images de skeleton
SKELETON = [
    './images/sprites/characters/skeleton/skeleton_up1.png',
    './images/sprites/characters/skeleton/skeleton_up2.png',
    './images/sprites/characters/skeleton/skeleton_right1.png',
    './images/sprites/characters/skeleton/skeleton_right2.png',
    './images/sprites/characters/skeleton/skeleton_down1.png',
    './images/sprites/characters/skeleton/skeleton_down2.png',
    './images/sprites/characters/skeleton/skeleton_left1.png',
    './images/sprites/characters/skeleton/skeleton_left2.png'
]

# *Images de king
KING = [
    './images/sprites/characters/king/king_up2.png',
    './images/sprites/characters/king/king_up2.png',
    './images/sprites/characters/king/king_right1.png',
    './images/sprites/characters/king/king_right2.png',
    './images/sprites/characters/king/king_down1.png',
    './images/sprites/characters/king/king_down2.png',
    './images/sprites/characters/king/king_left1.png',
    './images/sprites/characters/king/king_left2.png'
]

# *Liste des perso'
PLAYER_LIST = [MAGE, ADAM, BILL, NINJA, KNIGHT, PRINCESS, SKELETON, KING]

# *Tableau des joueurs
PLAYER_CHOICE = [
    random.choice(PLAYER_LIST)
]

# *Image de background du menu (159 au final)
BACKGROUND_MENU_IMAGE = []
for i in range(160):
    BACKGROUND_MENU_IMAGE.append('./images/background/galaxy_purple_{0:0>3}.png'.format(i))

# *Image de fumer d'apparition
MYSTH_APPEAR = []
for i in range(8):
    MYSTH_APPEAR.append('./images/sprites/others/mysth_appear/mysth_appear{0:0>2}.png'.format(i))

# *Image de la zone de combat
AREA_FLOOR = './images/sprites/others/wall0010.png'

# *Image de la balle d'energie
ENERGY_BALL = './images/sprites/others/trump-pixel.png'

# *Image de la balle d'energie qui explose
EXPLOSION = []
for i in range(10):
    EXPLOSION.append('./images/sprites/others/explosion/explosion{0}.png'.format(i))

# *Image de l'item
ITEM = './images/sprites/others/item.png'

# *Image de la tete de mort
SKULL_HEAD = './images/sprites/others/skull-head.png'

# !##################################
# !           FIN IMAGE             #
# !##################################


# !##################################
# !       DEBUT ZONE DE COMBAT      #
# !##################################
# Taille de la zone de combat
TOTAL_AREA_SIZE = SPRITE_SIZE * SIZE_MAP
if TOTAL_AREA_SIZE > SCREEN_HEIGHT:
    SIZE_MAP = 20
    TOTAL_AREA_SIZE = SPRITE_SIZE * SIZE_MAP

# Position x de la zone de combat
POSITION_X_AREA = int((SCREEN_WIDTH / 2) - (TOTAL_AREA_SIZE / 2))

# Position y de la zone de combat
POSITION_Y_AREA = int((SCREEN_HEIGHT / 2) - (TOTAL_AREA_SIZE / 2))
# !##################################
# !      FIN ZONE DE COMBAT         #
# !##################################


# !##################################
# !      DEBUT POSITIONS            #
# !       DES JOUEURS               #
# !##################################
# Positions de chaque joueur
POSITION_X_PLAYER_ONE = POSITION_X_AREA
POSITION_Y_PLAYER_ONE = POSITION_Y_AREA

POSITION_X_PLAYER_TWO = POSITION_X_PLAYER_ONE + ((SIZE_MAP * SPRITE_SIZE) - SPRITE_SIZE)
POSITION_Y_PLAYER_TWO = POSITION_Y_AREA

POSITION_X_PLAYER_THREE = POSITION_X_AREA
POSITION_Y_PLAYER_THREE = (TOTAL_AREA_SIZE - SPRITE_SIZE)
POSITION_Y_PLAYER_THREE += POSITION_Y_AREA

POSITION_X_PLAYER_FOUR = POSITION_X_PLAYER_ONE + ((SIZE_MAP * SPRITE_SIZE) - SPRITE_SIZE)
POSITION_Y_PLAYER_FOUR = POSITION_Y_PLAYER_THREE
# !##################################
# !      FIN POSITIONS              #
# !       DES JOUEURS               #
# !##################################