import pygame
import time
import math
import random
from constantes import (SIZE_MAP, AREA_FLOOR, SPRITE_SIZE, TEXT_FONT_MODS_MENU,
                        SCREEN_WIDTH, POSITION_Y_RECTANGLE, SCREEN_HEIGHT,
                        GREEN3, GREEN2, GREEN, POSITION_X_AREA, 
                        POSITION_Y_AREA, SPRITE_SIZE, TOTAL_AREA_SIZE,
                        POSITION_X_PLAYER_ONE, POSITION_Y_PLAYER_THREE,
                        ITEM, EXPLOSION, RED, SKULL_HEAD)



# !###############################
# !                              #
# !  Fonctions, etc...           #
# !                              #
# !###############################

# !Global variables

counter_for_stars = 0

# !Functions !


def display_area(screen, area_size=SIZE_MAP,
                 image=AREA_FLOOR, sprite_size=SPRITE_SIZE):
    """
    This function display the fight area
    the size is define by the parameter
    """

    # Taille de la zone de combat
    total_area_size = sprite_size * (area_size)
    # Position x de la zone de combat
    position_x_area = (SCREEN_WIDTH / 2) - (total_area_size / 2)
    # Position y de la zone de combat
    position_y_area = (SCREEN_HEIGHT / 2) - (total_area_size / 2)
    # Position x et y de la zone de combat
    area_position = [
        position_x_area,
        position_y_area
    ]

    image = pygame.image.load(image)
    image = pygame.transform.scale(image, (SPRITE_SIZE, SPRITE_SIZE))
    image = image.convert_alpha()

    # Affichage du sol de la zone de combat
    for x in range(area_size):
        pos_x = position_x_area
        for y in range(area_size):
            screen.blit(image, (pos_x, position_y_area))
            pos_x += SPRITE_SIZE
        position_y_area += SPRITE_SIZE

    return area_position


def matrix_background(screen):
    """
    Affiche le background derriere la zone de combat
    """
    global counter_for_stars

    if counter_for_stars is 4:
        # create locations of the stars when we animate the background
        star_field_slow = []
        star_field_medium = []
        star_field_fast = []

        # birth those plasma balls
        for slow_stars in range(50):
            star_loc_x = random.randrange(0, SCREEN_WIDTH)
            star_loc_y = random.randrange(0, SCREEN_HEIGHT)
            star_field_slow.append([star_loc_x, star_loc_y])

        for medium_stars in range(35):
            star_loc_x = random.randrange(0, SCREEN_WIDTH)
            star_loc_y = random.randrange(0, SCREEN_HEIGHT)
            star_field_medium.append([star_loc_x, star_loc_y])

        for fast_stars in range(15):
            star_loc_x = random.randrange(0, SCREEN_WIDTH)
            star_loc_y = random.randrange(0, SCREEN_HEIGHT)
            star_field_fast.append([star_loc_x, star_loc_y])

        # animate the stars
        for star in star_field_slow:
            star[1] += 1
            if star[1] > SCREEN_HEIGHT:
                star[0] = random.randrange(0, SCREEN_WIDTH)
                star[1] = random.randrange(-20, -5)
            pygame.draw.circle(screen, GREEN3, star, 3)

        for star in star_field_medium:
            star[1] += 4
            if star[1] > SCREEN_HEIGHT:
                star[0] = random.randrange(0, SCREEN_WIDTH)
                star[1] = random.randrange(-20, -5)
            pygame.draw.circle(screen, GREEN2, star, 2)

        for star in star_field_fast:
            star[1] += 8
            if star[1] > SCREEN_HEIGHT:
                star[0] = random.randrange(0, SCREEN_WIDTH)
                star[1] = random.randrange(-20, -5)
            pygame.draw.circle(screen, GREEN, star, 1)
        counter_for_stars = 0
    counter_for_stars += 1


def display_text(text, text_color, screen,
                 menu=False, position_x=0, position_y=0,
                 text_size=0, text_font=TEXT_FONT_MODS_MENU):
    """
    Affiche le texte souhaité à l'écran
    """
    # Couleur du texte
    text_color = text_color

    # Police et taille du texte
    font_text = pygame.font.Font(text_font, text_size)

    # Position x et y du text
    position_x_text = (SCREEN_WIDTH / 2) - (text_size * len(text) - 1) / 6 * 2
    position_y_text = POSITION_Y_RECTANGLE + 10
    if menu:
        position_x_text += position_x
        position_y_text += position_y
    else:
        position_x_text = position_x
        position_y_text = position_y

    text = font_text.render(text, True, text_color)
    screen.blit(text, (position_x_text, position_y_text))


def hit(player, position, position_joueurs):
    """
    Empeche la boule d'energie de sortir de la zone de combat
    ou de traverser une joueur
    """
    # Variables
    right_limit = POSITION_X_AREA + TOTAL_AREA_SIZE - SPRITE_SIZE
    left_limit = POSITION_X_AREA
    up_limit = POSITION_Y_AREA
    down_limit = POSITION_Y_AREA + (TOTAL_AREA_SIZE - SPRITE_SIZE)

    # Si la boule d'energie depasse a droite
    if (position[0] > right_limit):
        position[0] = right_limit
        return 1
    # Si la boule d'energie depasse a gauche
    elif (position[0] < left_limit):
        position[0] = left_limit
        return 1
    # Si lla boule d'energie depasse en haut
    elif (position[1] < up_limit):
        position[1] = up_limit
        return 1
    # Si la boule d'energie depasse en bas
    elif (position[1] > down_limit):
        position[1] = down_limit
        return 1
    else:
        for stuff in position_joueurs:
            if position == stuff[1]:
                return 1
        return 0


def display_credits():
    """
    Affiche les credits dans la console
    """
    print('.______   .______       ______          __   _______ .___________.')
    print('|   _  \  |   _  \     /  __  \        |  | |   ____||           |')
    print('|  |_)  | |  |_)  |   |  |  |  |       |  | |  |__   `---|  |----`')
    print('|   ___/  |      /    |  |  |  | .--.  |  | |   __|      |  |     ')
    print('|  |      |  |\  \----|  `--\'  | |  `--\'  | |  |____     |  |   ')
    print('| _| _______.| _______|\________ .__________________|__  |_____  _'
          '__      .______      _______ ')
    print('    /       | /  __  \  |   ____||           \   \  /  \  /   / / '
          '  \     |   _  \    |   ____|')
    print('   |   (----`|  |  |  | |  |__   `---|  |----`\   \/    \/   / /  '
          '^  \    |  |_)  |   |  |__   ')
    print('    \   \    |  |  |  | |   __|      |  |      \            / /  /'
          '_\  \   |      /    |   __|  ')
    print('.----)   |   |  `--\'  | |  |         |  |       \    /\    / /  __'
          '___  \  |  |\  \----|  |____ ')
    print('|_______/     \______/  |__|         |__|        \__/  \__/ /__/  '
          '   \__\ | _| `._____|_______|')
    time.sleep(3)
    print('Realise par..')
    time.sleep(.500)
    print('Ruben')
    time.sleep(.300)
    print('Nour')
    time.sleep(.300)
    print('Ritchy')
    time.sleep(.300)
    print('Miss Li')
    time.sleep(1)
    print('Ovila')
    time.sleep(3)
    print('Thanks for testing our game')
    print('it\'s our first video game and we\'ve liked that')
    print('Thanks to our school...')
    time.sleep(2)
    print('ETNA')


def show_balls(screen, balls_array, position_joueurs, position_explosion):
    """
    Montre et fait bouger les boules d'energies sur le terrain
    """
    # Index des boules d'energie sur le terrain
    index_balls = 0
    for image, position, direction in balls_array:
        # Fais apparaitre l'image a la position souhaite
        screen.blit(image, position)
        if direction is 0:
            balls_array[index_balls][1] = [
                position[0],
                position[1] - (SPRITE_SIZE / 2)
                ]
        elif direction is 2:
            balls_array[index_balls][1] = [
                position[0],
                position[1] + (SPRITE_SIZE / 2)
                ]
        elif direction is 3:
            balls_array[index_balls][1] = [
                position[0] - (SPRITE_SIZE / 2),
                position[1]
                ]
        elif direction is 1:
            balls_array[index_balls][1] = [
                position[0] + (SPRITE_SIZE / 2),
                position[1]
                ]
        # Savoir si quelque chose a ete touche (retourne 1 ou 0)
        success_hit = hit(index_balls, balls_array[index_balls][1], position_joueurs)

        # Si quelque chose est touche
        if success_hit is 1:
            # Prendre la position de l'endroit ou il y a eu un contact
            last_position = balls_array[index_balls][1]
            balls_array.pop(index_balls)
            position_explosion.append([0,last_position])
        index_balls += 1
    
    return balls_array, position_joueurs


def anime_explosion(screen, position_explosion):
    """
    Fait apparaitre l'animation d'explosion
    """
    for i, explosion in enumerate(position_explosion):
        position = explosion[1]
        image_explosion = pygame.image.load(EXPLOSION[explosion[0]])
        image_explosion = pygame.transform.scale(image_explosion, (SPRITE_SIZE,
                                                                    SPRITE_SIZE))
        screen.blit(image_explosion, position)
        explosion[0] += 1
        if explosion[0] >= len(EXPLOSION):
            position_explosion.pop(i)
    
    return position_explosion


def make_position_item():
    """
    Genere les positions des objets sur la carte
    """
    # Variables
    start_x = POSITION_X_AREA
    end_x = POSITION_X_PLAYER_ONE + ((SIZE_MAP * SPRITE_SIZE))

    start_y = POSITION_Y_AREA
    end_y = POSITION_Y_PLAYER_THREE

    # Liste qui va contenire tous les nombres correspondant a
    # des positions sur la map
    number_x = []  
    number_y = []
    
    # Boucle pour definir le point X
    for x in range(start_x, end_x, SPRITE_SIZE):
        number_x.append(x)
    
    # Boucle pour definir le point Y
    for y in range(start_y, end_y, SPRITE_SIZE):
        number_y.append(y)
    
    return random.choice(number_x), random.choice(number_y)


def action_item(items, players, screen):
    """
    Ajoute de l'energie a celui qui trouve un item et l'enleve de la carte
    """
    # On affiche les items sur la carte
    display_items(items, screen)

    # Verifier si un joueur se trouve sur la case d'un item
    for player in players:
        for item in items:
            if list(item) == list(player[1]):
                # Si un joueur se trouve sur un item on lui rajoute 10 d'energie
                player[2]['energy'] += 10
                # On retire l'item ramasse du tableau d'item
                items.remove(item)
                # On re-affiche les items sur la carte
                display_items(items, screen)

    
    return items, players


def display_items(items, screen):
    """
    Affiche les objets sur la carte
    """
    # Variable
    max_items = int(math.ceil(SIZE_MAP / 3))

    # Boucle sur chaque position dans le tableau items exemple => [[30,30], [250,10]...]
    # pour pouvoir ensuite les afficher sur la carte
    for position_item in items:
        item_image = pygame.image.load(ITEM)
        item_image = pygame.transform.scale(item_image, (SPRITE_SIZE, SPRITE_SIZE))
        item_image = item_image.convert_alpha()
        screen.blit(item_image, position_item)

    # Si il y a plus d'items que prevue on enleve un item
    # du tableau des positions
    if len(items) > max_items:
        # Genere un nombre aleatoire pour savoir
        # quel item enlever
        item = random.randint(0, max_items) - 1
        if item < 0:
            item = 0
        items.pop(item)


def show_players_status(players_array, screen):
    """
    Affiche l'energie et le stamina des joueurs au dessus de leur tete
    """
    for player in players_array:
        x = player[1][0] - 50
        y = player[1][1] - 30
        energy_and_action = str(player[2]['name']) + " | " + str(player[2]['energy']) + " | " + str(player[2]['action'])
        # Affiche l'energie et le stamina des joueurs au dessus de leur tete
        display_text(energy_and_action, RED, screen,position_x=x, 
                    position_y=y,
                    text_size=10)


def show_skull_head(screen, position):
    """
    Montre une tete de mort la ou le joueur est mort
    """
    image = pygame.image.load(SKULL_HEAD)
    image = pygame.transform.scale(image, (SPRITE_SIZE, SPRITE_SIZE))
    image = image.convert_alpha()

    screen.blit(image, position)


def create_player(string):
    """
    Creer un joueur
    """
    tab = string.split('|')
    name = ''
    position = []
    
    if(tab[0] == 'PC'):
        name = tab[1]
        position = tab[2].split(':')
    
    position[0] = POSITION_X_AREA + SPRITE_SIZE * int(position[0])
    position[1] = POSITION_Y_AREA + SPRITE_SIZE * int(position[1])

    return name, position


def show_front_of_me(string):
    """
    Mode vision
    """
    response = string.split('|')[0]
    players = string.split('|')[1]
    players = players[1:len(players)-1].split(',')

    for counter, player in enumerate(players):
        if len(player) is 0:
            players[counter] = 'null'