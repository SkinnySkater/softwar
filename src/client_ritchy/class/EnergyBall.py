import pygame
import sys
sys.path.insert(0, '..')
from constantes import (SPRITE_SIZE, ENERGY_BALL)

class EnergyBall():
    """
    Class pour instancier
    les boules d'energies
    """
    def __init__(self, position_ball_x, position_ball_y, direction):
        self.position = [
            position_ball_x,
            position_ball_y
        ]
        self.image = ENERGY_BALL
        self.image_now = ''
        self.__direction = direction

    def appear(self):
        """
        Faire apparaitre la boule d'energie a un point
        x et y donnés en parametre
        """
        appear = pygame.image.load(self.image)
        appear = pygame.transform.scale(appear, (SPRITE_SIZE,
                                                            SPRITE_SIZE))
        #first_appear.set_colorkey(WHITE)
        
        self.image_now = appear

        return self.image_now, self.position, self.__direction