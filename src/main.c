#include "server/notification.h"

#include <getopt.h>


int 	main(int argc, char **argv)
{
	s_config 	*config;
	config = init_config();
	int opt= 0;
    int v = FALSE, rport = 1, pport = 1, cycle = 500000, size = 5, lflag = 0;
    char log[64];
    memset(log, '\0', sizeof(log));

    static struct option long_options[] = {
        {"verbose", 	  no_argument,     0,  'v' },
        {"help", 	      no_argument,     0,  'h' },
        {"rep-port",    required_argument, 0,  'r' },
        {"pub-port",    required_argument, 0,  'p' },
        {"cycle",       required_argument, 0,  'c' },
        {"size",        required_argument, 0,  's' },
        {"log",         required_argument, 0,  'l' },
        {0,           0,                 0,  0   }
    };

    int long_index =0;
    while ((opt = getopt_long(argc, argv,"hvp:l:r:c:s", 
                   long_options, &long_index )) != -1) {
        switch (opt) {
			case 'v' : v = TRUE;
				config->verbose = v;
				break;
			case 'h' : p_usage();
				exit(0);
				break;
			case 'r' :
				if (!is_nbr(optarg)){
					printf("rep-port Required an INT.\n");
					exit(EXIT_FAILURE);
				}
				rport = atoi(optarg);
				strcpy(config->rep_port, optarg);
				break;
			case 'p' :
				if (!is_nbr(optarg)){
					printf("pub-port Required an INT.\n");
					exit(EXIT_FAILURE);
				}
				pport = atoi(optarg);
				strcpy(config->pub_port, optarg);
				break;
			case 'c' :
				if (!is_nbr(optarg)){
					printf("cycle Required an INT.\n");
					exit(EXIT_FAILURE);
				}
				cycle = atoi(optarg);
				config->cycle = cycle;
				break;
			case 's' :
				if (!is_nbr(optarg)){
					printf("size Required an INT.\n");
					exit(EXIT_FAILURE);
				}
				size = atoi(optarg);
				config->size = size;
				break;
			case 'l' : strcpy(log, optarg);
				lflag = 1;
				strcpy(config->log, log);
				break;
			default: p_usage();
				exit(EXIT_FAILURE);
        }
    }
    if (cycle <= 0 || !is_in_port_range(rport) || !is_in_port_range(pport)
    	|| size < MIN_MAP_SIZE){
        p_usage();
        exit(EXIT_FAILURE);
    }
    if (!lflag){
    	printf("--log or -l file is mandatory\n");
        exit(EXIT_FAILURE);
    }
    if (!store_log_file(log)){
    	printf("Can't CREATE The file: %s\n", log);
        exit(EXIT_FAILURE);
    }

	run(config);
	return 0;
}