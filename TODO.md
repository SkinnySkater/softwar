git clone https://SkinnySkater@bitbucket.org/SkinnySkater/softwar.git

#install:
echo "deb http://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/ ./" >> /etc/apt/sources.list
wget https://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/Release.key -O- | sudo apt-key add
apt-get install libczmq-dev

echo "deb http://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/ ./" >> /etc/apt/sources.list
wget https://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/Release.key -O- | sudo apt-key add
apt-get install libzmq3-dev


#PROCESSUS

    Le jeu ne peut commencer que lorsque quatre clients sont connectés											[V]

    La connexion d’un client entraîne son apparition sur la map													[X]

    Les clients sont consécutivement placés dans le coin supérieur gauche
    , dans le coin supérieur droit, dans le coin inférieur gauche et enfin
     dans le coin inférieur droit																				[V]
    En plus de la position des processus sur la map, le serveur doit stocker :
        leur orientation																						[V]
        une signature numérique arbitraire et unique sous la forme :
         
         #0x00 (les deux derniers digits de l’exemple doivent être remplacés 									[V]
         par un nombre unique et aléatoire composé de deux digits)

    C’est le client qui devra envoyer cette signature, qui correspondra à 										[V]
    l’identité de sa socket, à l’aide de la commande identify (voir la RFC)

    Si jamais l’identifiant est déjà pris, le serveur répondra alors au client : ko|identity already exists		[V]

    Si jamais 4 joueurs sont déjà connectés, le serveur répondra alors au client : ko|game full					[V]

    Les processus ont besoin d’énergie pour continuer à fonctionner 											

        Au début du jeu, chaque processus possède 50 unités d’énergie 											[V]
        Au début de chaque cycle, tous les processus perdent 2 unités d’énergies								[X]
        Un processus ne peut posséder qu’un maximum de 100 unités d’énergie, au-delà 							[V]
        de cette limite, il s’autodétruit



/**
 ** ACTION OF PROCESSUS
/**
IDENTIFY			[ok]
FORWARD 			[ok]
BACKWARD			[ok]
LEFTWD				[ok]
RIGHTWD				[ok]
RIGHT				[ok]
LEFT				[ok]
LOOKING				[ok]
GATHER 				[ok]
WATCH 				[ok]
ATTACK 				[ok]
SELFSTAT			[ok]
INSPECT				[ok]
NEXT				[ok]
JUMP				[ok]

Rajouter les publication lorsque l energy tombe <= 0 puis eliminer le player						[V]
Rajouter dans le cycle update action point des players et de l energy sur la map 					[V]
Check energy and remove player from list or add dead field 											[V]
ADD publication for energy and players 																[V]


FIX CYCLE       [V]
FIX INIT MAP    [V]
